/*
* CART JS
*/
var removeItem = (cartId) => {
    Swal.fire({
        title: 'Are you sure?', text: "You want to remove this item from your cart?", type: 'question',
        showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, remove Item!'
      }).then(res => {

        if(res.value) {
            axios.delete('/api/cart/item/' + cartId).then(res => {
                location.reload();
            }).catch(err => {
                console.log(err);
                Swal.fire('Sorry!', 'Unable to remove the item in your cart at this moment.', 'error');
            });
        }
    });
};

var updateQuantity = (cartId, qty, opt) => {

    if(qty < 1) {
        this.removeItem(cartId);
    } else {
        axios.patch('/api/cart/item/' + cartId + '/' + opt + '/' + qty).then(res => {
            location.reload();
        }).catch(err => {
            if(err.response.status == 400) {
                toastr.warning('Sorry! You have reached maximum quantity allowed.');
            } else {
                toastr.error('Sorry! Unable to add item on your cart this time.');
            }
            setTimeout(() => { location.reload() }, 1000);
        });
    }
};

$(document).ready(() => {

    // on quantity less is click
    $('.count-less').on('click', (e) => {
       let qty = parseInt($(e.target).closest('div').find('.txtQty').val()) - 1;
       let cartId = $(e.target).closest('div').prop('id');

       updateQuantity(cartId, qty, 'less');
    });
    // on quantity add is click
    $('.count-more').on('click', (e) => {
        let qty = parseInt($(e.target).closest('div').find('.txtQty').val()) + 1;
        let cartId = $(e.target).closest('div').prop('id');

        updateQuantity(cartId, qty, 'more');
    });

    $('.txtQty').on('keypress', (e) => {
        if(e.which == 13) {
            let qtyVal = $(e.target).val();
            let cartId = $(e.target).closest('div').prop('id');

            // maximum allowed input is 20
            if(parseInt(qtyVal) >= 20) {
                $(e.target).val('20');
                qtyVal = 20;
            }
            
            updateQuantity(cartId, qtyVal, 'input');
        }
    });

    // on remove cart item
    $('.btnRemove').on('click', (e) => {
        console.log($(e.target).prop('id'));
        removeItem($(e.target).prop('id'));
    });
});