/*
* JS of Shop Product Listing
*/ 

var unif_key = 'uni_M', unif_size = 'MEDIUM';

var updateUniformPrice = (e) => {

    const price = $(e.target).closest('span').text();
    const size =  $(e.target).closest('span').next().text();

    $(e.target).closest('section').find('.lblCartBPrice').text(price);
    $(e.target).closest('section').find('.lblCartAPrice').text(price);
    $(e.target).closest('section').find('.lblfrontPrice').text(price);
    $(e.target).closest('section').find('.lblUnifSize').text(size);

};

$(document).ready(() => {

    $('.btn_size').on('click', function(e) {
        e.preventDefault();

        unif_key = $(this).data('unifsize');
        updateUniformPrice(e);
    });
    // when add to cart is click
    $('.btnAddToCart').on('click', function(e) {
        e.preventDefault();

        const prodType = $(e.target).closest('div').data('type');
        const prodId = $(e.target).parent().prop('id');

        axios.post('/api/student/cart', {prodId: prodId, prodType: prodType, var_key: unif_key })
        .then((res) => {
            toastr.success('You have added an item to your cart.');
            location.reload();
        })
        .catch(err => {
            if(err.response.status == 400) {
                toastr.warning('Sorry! You have reached maximum quantity allowed.');
            } else {
                toastr.error('Sorry! Unable to add item on your cart this time.');
            }
        })
    });
});