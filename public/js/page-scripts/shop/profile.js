// update student password
var updatePassword = (data) => {
    Swal.fire({
        title: "Update Password",
        text: "Are you sure you want to update your password?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, Update password."
    }).then(function(e) {
        if(e.value) {
            axios.post('/api/student/password', data).then( res => {
                Swal.fire('Great!', 'Password has been updated. You may now use your new password.', 'success');
            }).catch(err => {
                Swal.fire('Sorry!', 'Unable to update password.', 'error');
            });
        }
    });
};

$(document).ready(()=> {

    // when update password button is click
    $('#btnUpdatePass').on('click', (e)=> {
        updatePassword($('#frmUpdatePass').serialize());
    });
});