//get admin list
var getAdmins= {
    init:function() {
        var t;
        t=$(".m_datatable").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:"/api/admin/list",
                        method: 'GET',
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "AdminId", title: "#", sortable: !1, selector: !1, textAlign: "center", template: function(t, idx, tbl) {
                    var rowNum = (idx + 1) + (tbl.getCurrentPage() - 1) * tbl.getPageSize();
                    return '<label id="'+ t.id + '">'+rowNum + '</label>';
                }
            }
            , {
                field: "name", title: "Full Name", textAlign: "center"
            }
            , {
                field: "email", title: "Email Address", textAlign: "center"
            }
            , {
                field: "username", title: "Username", textAlign: "center"
            }
            , {
                field: "created_at", title: "Added At", textAlign: "center"
            }]
        }
        ),
        $("#m_form_status").on("change", function() {
            t.search($(this).val(), "Status")
        }
        ),
        $("#m_form_type").on("change", function() {
            t.search($(this).val(), "Type")
        }
        ),
        $("#m_form_status, #m_form_type").selectpicker()
    },
    reload:function() {
        $(".m_datatable").mDatatable("reload");
    }
};

$(document).ready(() => {

    getAdmins.init();

    $('#btnAddAdmin').on('click', (e)=> {

        axios.post('/api/settings/admin', $('#frmNewAdmin').serialize()).then(res => {
            getAdmins.reload();
            $('#mdlAddAdmin').modal('hide');
            swal('Great!', 'A new admin has been created.', 'success');
        }).catch(err => {
            swal('Sorry!', 'Unable to add admin this time.', 'error');
        });
    });
});