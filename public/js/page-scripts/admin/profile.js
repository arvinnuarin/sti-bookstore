// update admin password
var updatePassword = (data) => {
    swal({
        title: "Update Password",
        text: "Are you sure you want to update your password?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, Update password."
    }).then(function(e) {
        if(e.value) {
            axios.post('/api/settings/admin/password', data).then( res => {
                swal('Great!', 'Password has been updated. You may now use your new password.', 'success');
            }).catch(err => {
                swal('Sorry!', 'Unable to update password', 'error');
            });
        }
    });
};

$(document).ready(() => {

    // update password
    $('#btnUpdatePass').on('click', (e)=> {
        e.preventDefault();
        updatePassword($('#frmUpdatePass').serialize());
    });
});