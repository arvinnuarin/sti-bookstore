var pendingOrdersTable = function(subcat) {
    return $(".m_datatable").mDatatable( {
        data: {
            type:"remote", source: {
                read: {
                    url:"/api/orders/" + subcat ,
                    method: 'GET',
                }
            }
            , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0
        }
        , layout: {
            scroll: !1, footer: !1
        }
        , sortable:!0, pagination:!0, toolbar: {
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 50, 100]
                }
            }
        }
        , search: {
            input: $("#generalSearch")
        }
        , columns:[ {
            field: "OrderId", title: "#", sortable: !1, selector: !1, width:30, textAlign: "center", template: function(t, idx, tbl) {
                var rowNum = (idx + 1) + (tbl.getCurrentPage() - 1) * tbl.getPageSize();
                return '<label id="'+ t.id + '">'+rowNum + '</label>';
            }
        }
        , {
            field: "order_no", title: "Order No", textAlign: "center"
        }
        , {
            field: "stud_no", title: "Student No", textAlign: "center"
        }
        , {
            field: "name", title: "Student Name", textAlign: "center"
        }
        , {
            field: "summary", title: "Order Amount", textAlign: "center", template:function(t) {
                
                summary = JSON.parse(t.summary);
                return '<span><h4>'+ accounting.formatMoney(summary.total, "₱ ", 2) + '</h4></span>';
            }
        }
        , {
            field: "created_at", title: "Transaction Date", textAlign: "center"
        }
        , {
            field:"isFulfilled", title:"Status", textAlign: "center", template:function(t) {
                var e= {
                    true: {
                        title: "Completed", class: " m-badge--info"
                    },
                    false: {
                        title: "Pending", class: " m-badge--danger"
                    }
                };
                return'<span class="m-badge '+e[t.isFulfilled].class+' m-badge--wide">'+e[t.isFulfilled].title+"</span>"
            }
        }
        , {
            field:"Actions", width:110, title:"Actions", textAlign: "center", sortable:!1, overflow:"visible", template:function(t, e, a) {
                return '<a href="/sti-adm/transactions/orders/pdf/' + t.id +'" class="btnViewOrder m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Order"><i class="la la-print"></i></a>'
                + '<a class="btnUpdOrder m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Fulfill Order"><i class="la la-leaf"></i></a>';
            }
        }]
    });
}

var completedOrdersTable = function(subcat) {
    return $(".m_datatable").mDatatable( {
        data: {
            type:"remote", source: {
                read: {
                    url:"/api/orders/" + subcat ,
                    method: 'GET',
                }
            }
            , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0
        }
        , layout: {
            scroll: !1, footer: !1
        }
        , sortable:!0, pagination:!0, toolbar: {
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 50, 100]
                }
            }
        }
        , search: {
            input: $("#generalSearch")
        }
        , columns:[ {
            field: "OrderId", title: "#", sortable: !1, selector: !1, width:30, textAlign: "center", template: function(t, idx, tbl) {
                var rowNum = (idx + 1) + (tbl.getCurrentPage() - 1) * tbl.getPageSize();
                return '<label id="'+ t.id + '">'+rowNum + '</label>';
            }
        }
        , {
            field: "order_no", title: "Order No", textAlign: "center"
        }
        , {
            field: "stud_no", title: "Student No", textAlign: "center"
        }
        , {
            field: "name", title: "Student Name", textAlign: "center"
        }
        , {
            field: "created_at", title: "Transaction Date", textAlign: "center"
        }
        , {
            field: "updated_at", title: "Fulfilled Date", textAlign: "center"
        }
        , {
            field:"isFulfilled", title:"Status", textAlign: "center", template:function(t) {
                var e= {
                    true: {
                        title: "Completed", class: " m-badge--info"
                    },
                    false: {
                        title: "Pending", class: " m-badge--danger"
                    }
                };
                return'<span class="m-badge '+e[t.isFulfilled].class+' m-badge--wide">'+e[t.isFulfilled].title+"</span>"
            }
        }
        , {
            field:"Actions", width:110, title:"Actions", textAlign: "center", sortable:!1, overflow:"visible", template:function(t, e, a) {
                return '<a href="/sti-adm/transactions/orders/pdf/' + t.id +'" class="btnViewOrder m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Order"><i class="la la-print"></i></a>';
            }
        }
        ]
    });
}

var getOrders= {
    init:function(subcat) {
        var t;

        t = subcat == 'pending' ? pendingOrdersTable(subcat) : completedOrdersTable(subcat);

        $("#m_form_status").on("change", function() {
            t.search($(this).val(), "Status")
        }
        ),
        $("#m_form_type").on("change", function() {
            t.search($(this).val(), "Type")
        }
        ),
        $("#m_form_status, #m_form_type").selectpicker()
    },
    reload:function() {
        $(".m_datatable").mDatatable("reload");
    }
};

$(document).ready(() => {

     //Get Book Grade
     var orderType = $(document).find('[data-field="order"]').prop('id');
     // Initialize table
     getOrders.init(orderType);

    // on fulfill order
    $(document).on('click', '.btnUpdOrder', (e) => {
        e.preventDefault();

        var id = $(e.target).closest('.m-datatable__row').find('[data-field="OrderId"] label:last-child').prop('id');
        
        swal({
            title: "Fulfill Order",
            text: "Click 'Fulfill Order' to facilitate fulfillment of this order",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Fulfill Order"
        }).then(function(e) {
            
            if(e.value) {
                axios.patch('/api/orders/id/'+id)
                .then(resp => {
                    swal('Great!', 'Book Status has been updated.', 'success');
                    // reload datatable
                    getOrders.reload();
                }).catch(err => {
                    console.error(err);
                    swal('Something went wrong', 'Unable to update order status. Please try again later.', 'error');
                });
            }
        }).catch(err => {
            swal('Something went wrong', 'Unable to update order status. Please try again later.', 'error');
        });

    });
});