var getSupplies= {
    init:function() {
        var t;
        t=$(".m_datatable").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:"/api/product/supplies/supply",
                        method: 'GET',
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "SupplyId", title: "#", sortable: !1, width: 30, selector: !1, width: 20, textAlign: "center", template: function(t, idx, tbl) {
                    var rowNum = (idx + 1) + (tbl.getCurrentPage() - 1) * tbl.getPageSize();
                    return '<label id="'+ t.id + '">'+rowNum + '</label>';
                }
            }
            , {
                field:"images", title:"Image", textAlign: "center", filterable:!1, width:100, template: function(t) {
                     return '<span><img width="60" class=" m--marginless" src="'+ t.imgsrc + '"></span>';
                }
            }
            , {
                field: "name", title: "Name", textAlign: "center", width: 200
            }
            , {
                field: "price", title: "Price", width: 100, template: function(t) {
                    return '<span><h4>'+ accounting.formatMoney(t.price, "₱ ", 2) + '</h4></span>';
                }
            }
            , {
                field: "stock", title: "Stocks",  textAlign: "center", template: function(t) {
                    return '<span><h4 class="text-primary">'+ t.stock+ '</h4></span>';
                }
            }
            , {
                field:"isAvailable", title:"Status", template:function(t) {
                    var e= {
                        true: {
                            title: "Available", class: " m-badge--info"
                        },
                        false: {
                            title: "Unavailable", class: " m-badge--danger"
                        }
                    };
                    var isAvailable = t.stock >=1 ? true : false;
                    return'<span class="m-badge '+e[isAvailable].class+' m-badge--wide">'+e[isAvailable].title+"</span>"
                }
            }
            , {
                field:"Actions", width:110, title:"Actions", sortable:!1, overflow:"visible", template:function(t, e, a) {
                    return '<a class="btnUpdInfo m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Supply Details"><i class="la la-edit"></i></a>'
                    + '<a class="btnRm m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Remove Supply"><i class="la la-trash"></i></a>'
                }
            }
            ]
        }
        ),
        $("#m_form_status").on("change", function() {
            t.search($(this).val(), "Status")
        }
        ),
        $("#m_form_type").on("change", function() {
            t.search($(this).val(), "Type")
        }
        ),
        $("#m_form_status, #m_form_type").selectpicker()
    },
    reload:function() {
        $(".m_datatable").mDatatable("reload");
    }
};

var fillUpmodal = function(data){
    // set book details and show update book detail modal
    $('[data-field="uuid"]').prop('id', data.id);
    $('#mdlTitle').text(data.title);
    $('#txtName').val(data.name);
    $('#txtPrice').val(data.price);
    $('#txtStock').val(data.stock);
    $('#mdlSupply').modal('show');
};

$(document).ready(()=> {

    //initialize table
    getSupplies.init();

    $('#btnToggleModal').on('click', () => { //show add new school supply modal
        fillUpmodal({id: 'none', title: ' Add School Supply', name: '', price: 0, stock: 1});
    });

    // when button add supply save is clicked
    $('#btnSupplySave').on('click', () => {

        var prodId = $('[data-field="uuid"]').prop('id');

        if(prodId === 'none') { // add school supply

            let formData = new FormData();
            formData.append('product', $('#prdImage').prop('files')[0]);

            axios.post('/api/product/image', formData, { headers: { 'Content-Type': 'multipart/form-data' }})
            .then(r =>  {
                axios.post('/api/product/supply', {
                    name: $('#txtName').val(), price: $('#txtPrice').val(), imgsrc: r.data, stock: $('#txtStock').val()
                  })
                  .then(function (res) {
    
                    $('#mdlSupply').modal('hide');
                    swal('Great!', 'A new school supply has been added.', 'success');
                    // reload datatable
                    getSupplies.reload();
                  })
                  .catch(function (error) {
                    console.log(error);
                    swal('Something went wrong', 'Unable to add this item. Please check your input.', 'error');
                  });
            }).catch(e => {
                swal('Something went wrong', 'Unable to add this item. Please check your input.', 'error');
            });
        } else {
            axios.patch('/api/product/supply', {
                id: prodId, name: $('#txtName').val(), price: $('#txtPrice').val(), stock: $('#txtStock').val()
              })
              .then(function (res) {

                $('#mdlSupply').modal('hide');
                swal('Great!', 'Item has been updated.', 'success');
                // reload datatable
                getSupplies.reload();
              })
              .catch(function (error) {
                console.log(error);
                swal('Something went wrong', 'Unable to update this item. Please check your input.', 'error');
              });
        }
    });

    //when button update Info is clicked
    $(document).on('click', '.btnUpdInfo', (e) => {
        e.preventDefault();

        var row = $(e.target).closest('.m-datatable__row');
        var prodId = row.find('[data-field="SupplyId"] label:last-child').prop('id');
        var name = row.find('[data-field="name"] span:last-child').text();
        var price = accounting.unformat(row.find('[data-field="price"] h4:last-child').text());
        var stock = row.find('[data-field="stock"] h4:last-child').text();

        fillUpmodal({id: prodId, title: ' Update School Supply', name: name, price: price, stock: stock});
    });

     //when button remove Supply is clicked
     $(document).on('click', '.btnRm', (e) => {
        e.preventDefault();
        var id = $(e.target).closest('.m-datatable__row').find('[data-field="SupplyId"] label:last-child').prop('id');

        swal({
            title: "Are you sure?",
            text: "You want to remove this item? This will delete the school supply from students cart too.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, remove item."
        }).then(function(e) {

            if(e.value) {
                axios.delete('/api/product/id/'+ id)
                .then(resp => {
                    swal('Success!', 'This item has been removed from the system.', 'success');
                    // reload datatable
                    getSupplies.reload();
                }).catch(err => {
                    console.error(err);
                    swal('Something went wrong', 'Unable to remove this item. Please try again later.', 'error');
                });
            }
        });
    });
});
