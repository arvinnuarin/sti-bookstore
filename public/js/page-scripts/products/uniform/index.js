var getUniforms= {
    init:function(type) {
        var t;
        t=$(".m_datatable").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:"/api/product/uniforms/"+ type,
                        method: 'GET',
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "UnifId", title: "#", sortable: !1, width: 30, selector: !1, textAlign: "center", width: 20, template: function(t, idx, tbl) {
                    var rowNum = (idx + 1) + (tbl.getCurrentPage() - 1) * tbl.getPageSize();
                    return '<label id="'+ t.id + '">'+rowNum + '</label>';
                }
            }
            , {
                field:"images", title:"Image", textAlign: "center", filterable:!1, width:100, template: function(t) {
                     return '<span><img width="60" class=" m--marginless" src="'+ t.imgsrc + '"></span>';
                }
            }
            , {
                field: "name", title: "Name", textAlign: "center", width: 200
            },
            {
                field: "course", title: "Course", textAlign: "center", width: 100, template: function(t) {
                    return '<span>'+ JSON.parse(t.attrib).course + '</span>'
                }
            },
            {
                field: "sizes", title: "Sizes", textAlign: "center", width: 100, template: function(t) {
                    var sizes = JSON.parse(t.attrib).details;
                    var sizeEm="";
                    sizes.forEach(size => {
                        sizeEm += "<span class='m-badge m-badge--primary m-badge--wide'>" + size.size.name + "</span>";
                    });
                    return '<span>'+ sizeEm + '</span>'
                }
            },
            {
                field: "price", title: "Price", width: 100, template: function(t) {
                    return '<span><h4>'+ accounting.formatMoney(t.price, "₱ ", 2) + '</h4></span>';
                }
            }
            , {
                field:"Actions", width:80, title:"Actions", sortable:!1, overflow:"visible", template:function(t, e, a) {
                    return '<a class="btnUpdInfo m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Uniform Details"><i class="la la-edit"></i></a>'
                    + '<a class="btnRm m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Remove Uniform"><i class="la la-trash"></i></a>'
                }
            }
            ]
        }
        ),
        $("#m_form_status").on("change", function() {
            t.search($(this).val(), "Status")
        }
        ),
        $("#m_form_type").on("change", function() {
            t.search($(this).val(), "Type")
        }
        ),
        $("#m_form_status, #m_form_type").selectpicker()
    },
    reload:function() {
        $(".m_datatable").mDatatable("reload");
    }
};

var fillUpmodal = function(data){
    // set book details and show update book detail modal
    $('[data-field="uuid"]').prop('id', data.id);
    $('#mdlTitle').text(data.title);
    $('#sltCourse').val(data.course).change();
    $("input[name='gender'][value=" + data.gender + "]").prop('checked', true);
    $('#txtName').val(data.name);

    if(data.id !== 'none') {
        (data.attrib.details).forEach(d => { //set price for each sizes
            $('#'+d.size.key).val(d.price);
            $('#stk_'+d.size.key).val(d.stock);
        });
    }
    $('#mdlUniform').modal('show');
};

// document ready
$(document).ready(() => {

     //Get Uniform Type
     var uniformType = $(document).find('[data-field="type"]').prop('id');

    // Get Uniform
    getUniforms.init(uniformType);

    $('#btnToggleModal').on('click', () => { //show add new uniform modal
        fillUpmodal({id: 'none', title: ' Add Uniform', name: '', course: 'GEN', gender: 'Unisex'});
    });

    // when save uniform is clicked
    $('#btnUniformSave').on('click', () => {

        var details = [];
        var sizes = [{key: 'uni_S', name: 'Small'}, {key: 'uni_M', name: 'Medium'},
        {key: 'uni_L', name: 'Large'}, {key: 'uni_XL', name: 'XL'}, {key: 'uni_2XL', name: '2XL'},
        {key: 'uni_3XL', name: '3XL'}];
        var totalStock = 0;

        $(".uniPrice").each(function() {

            var stock = $(this).closest('td').next('td').find('.uniStock').val();
            var price = $(this).val(), key = $(this).prop('id');
            if((stock > 0) && (price != "" || price > 10)) {
                details.push({size: _.find(sizes, {key}), price: price, stock: stock})
            }
            totalStock += parseInt(stock);
        });
        
        var prodId = $('[data-field="uuid"]').prop('id');

        if( prodId === 'none') { //create uniform
            try {

                let formData = new FormData();
                formData.append('product', $('#prdImage').prop('files')[0]);

                axios.post('/api/product/image', formData, { headers: { 'Content-Type': 'multipart/form-data' }})
                .then(r =>  {

                    axios.post('/api/product/uniform', {
                        name: $('#txtName').val(), course: $('#sltCourse :selected').val(),
                        type: uniformType, gender: $("input[name='gender']:checked").val(),
                        details: details, price: details[0].price, imgsrc: r.data, stock: totalStock
                      })
                      .then(function (res) {
                        $('#mdlUniform').modal('hide');
                        swal('Great!', 'A new uniform  been added.', 'success');
                        // reload datatable
                        getUniforms.reload();
                      })
                      .catch(function (error) {
                        console.log(error);
                        swal('Something went wrong', 'Unable to add uniform. Please check your input.', 'error');
                      });
                }).catch(err => {
                    swal('Something went wrong', 'Unable to add uniform. Please check your input.', 'error');
                });
            } catch(e) {
                swal('Something went wrong', 'Unable to add uniform. Please check your input.', 'error');
            }
        } else {
            try {
                axios.patch('/api/product/uniform', {

                    id: prodId, name: $('#txtName').val(), course: $('#sltCourse :selected').val(),
                    type: uniformType, gender: $("input[name='gender']:checked").val(),
                    details: details, price: details[0].price, stock: totalStock
                  })
                  .then(function (res) {

                    $('#mdlUniform').modal('hide');
                    swal('Great!', 'Uniform has been updated.', 'success');
                    // reload datatable
                    getUniforms.reload();
                  })
                  .catch(function (error) {
                    console.log(error);
                    swal('Something went wrong', 'Unable to update uniform. Please check your input.', 'error');
                  });
            } catch(e) {
                swal('Something went wrong', 'Unable to add uniform. Please check your input.', 'error');
            }
        }
    });

    //when button remove uniform is clicked
    $(document).on('click', '.btnRm', (e) => {
        e.preventDefault();
        var id = $(e.target).closest('.m-datatable__row').find('[data-field="UnifId"] label:last-child').prop('id');

        swal({
            title: "Are you sure?",
            text: "You want to remove this uniform? This will delete the uniform from students cart too.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, remove uniform."
        }).then(function(e) {

            if(e.value) {
                axios.delete('/api/product/id/'+ id)
                .then(resp => {
                    swal('Success!', 'Uniform has been removed from the system.', 'success');
                    // reload datatable
                    getUniforms.reload();
                }).catch(err => {
                    console.error(err);
                    swal('Something went wrong', 'Unable to remove this uniform. Please try again later.', 'error');
                });
            }
        });
    });

    //when button update Info is clicked
    $(document).on('click', '.btnUpdInfo', (e) => {
        e.preventDefault();

        var row = $(e.target).closest('.m-datatable__row');
        var prodId = row.find('[data-field="UnifId"] label:last-child').prop('id');

        axios.get('/api/product/search/id/' + prodId).then((res) => {

            var data = res.data;
            var attribs = JSON.parse(data.attrib);

            fillUpmodal({title: ' Update Uniform Details', id: data.id, name: data.name, course: attribs.course,
            gender: attribs.gender, attrib: attribs});

        }).catch(err => {
            swal('Something went wrong', 'Unable to update uniform. Please try again later.', 'error');
        });
    });

});
