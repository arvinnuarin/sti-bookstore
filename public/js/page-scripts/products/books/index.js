var getBooks= {
    init:function(subcat) {
        var t;
        t=$(".m_datatable").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:"/api/product/books/" + subcat ,
                        method: 'GET',
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "BookId", title: "#", sortable: !1, selector: !1, textAlign: "center", width: 20, template: function(t, idx, tbl) {
                    var rowNum = (idx + 1) + (tbl.getCurrentPage() - 1) * tbl.getPageSize();
                    return '<label id="'+ t.id + '">'+rowNum + '</label>';
                }
            }
            , {
                field:"images", title:"Image", textAlign: "center", filterable:!1, width:150, template: function(t) {
                     return '<span><img width="60" class=" m--marginless" src="'+ t.imgsrc + '"></span>';
                }
            }
            , {
                field: "name", title: "Book Name", textAlign: "center"
            }
            , {
                field: "attrib", title: "Author", width: 100, template: function(t) {
                    return '<span class="author">'+ JSON.parse(t.attrib).author + '</span>'
                }
            }
            , {
                field: "price", title: "Price", width: 100, template: function(t) {
                    return '<span><h4>'+ accounting.formatMoney(t.price, "₱ ", 2) + '</h4></span>';
                }
            }
            , {
                field: "stock", title: "Stocks",  textAlign: "center", template: function(t) {
                    return '<span><h4 class="text-primary">'+ t.stock+ '</h4></span>';
                }
            }
            , {
                field:"isAvailable", title:"Status", template:function(t) {
                    var e= {
                        true: {
                            title: "Available", class: " m-badge--info"
                        },
                        false: {
                            title: "Unavailable", class: " m-badge--danger"
                        }
                    };
                    var isAvailable = t.stock >=1 ? true : false;
                    return'<span class="m-badge '+e[isAvailable].class+' m-badge--wide">'+e[isAvailable].title+"</span>"
                }
            }
            , {
                field:"Actions", width:110, title:"Actions", sortable:!1, overflow:"visible", template:function(t, e, a) {
                    return '<a class="btnUpdInfo m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Book Details"><i class="la la-edit"></i></a>'
                    + '<a class="btnRmBook m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Remove Book"><i class="la la-trash"></i></a>'
                }
            }
            ]
        }
        ),
        $("#m_form_status").on("change", function() {
            t.search($(this).val(), "Status")
        }
        ),
        $("#m_form_type").on("change", function() {
            t.search($(this).val(), "Type")
        }
        ),
        $("#m_form_status, #m_form_type").selectpicker()
    },
    reload:function() {
        $(".m_datatable").mDatatable("reload");
    }
};

var fillUpmodal = function(data){
     // set book details and show update book detail modal
     $('[data-field="uuid"]').prop('id', data.id);
     $('#mdlTitle').text(data.title);
     $('#txtBkName').val(data.name);
     $('#txtBkAuthor').val(data.author);
     $('#txtBkPrice').val(data.price);
     $('#txtBkStock').val(data.stock);
     $('#mdlNewBook').modal('show');
};

// When the page is ready
$(document).ready(() => {

    //Get Book Grade
    var bookGrade = $(document).find('[data-field="grade"]').prop('id');
    // Initialize table
    getBooks.init(bookGrade);

    // show modal
    $('#btnToggleModal').on('click', () => {
        fillUpmodal({id: 'none', title: ' Add New Book', name: '', author: '', price: 0 , stock: 1});
    });

    //Add new book
    $('#btnAddBookSave').on('click', ()=> {

        var prodId = $('[data-field="uuid"]').prop('id');

        if(prodId === 'none') { //add book
           
            let formData = new FormData();
            formData.append('product', $('#prdImage').prop('files')[0]);

            axios.post('/api/product/image', formData, { headers: { 'Content-Type': 'multipart/form-data' }})
            .then(function (r) {
                axios.post('/api/product/book', {
                    name: $('#txtBkName').val(),  author: $('#txtBkAuthor').val(),
                    grade: bookGrade, price: $('#txtBkPrice').val(), imgsrc: r.data, stock: $('#txtBkStock').val()
                })
                .then(function (res) {
                    $('#mdlNewBook').modal('hide');
                    swal('Great!', 'A new book has been added.', 'success');
                    // reload datatable
                    getBooks.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    swal('Something went wrong', 'Unable to add book. Please check your input.', 'error');
                });
            })
            .catch(function (e) {
                console.log(e);
                swal('Something went wrong', 'Unable to add book. Please check your input.', 'error');
            });
        } else { // update book
            axios.patch('/api/product/book', {
                id: prodId, name: $('#txtBkName').val(),  author: $('#txtBkAuthor').val(),
                grade: bookGrade, price: $('#txtBkPrice').val(), stock: $('#txtBkStock').val(),
              })
              .then(function (res) {

                $('#mdlNewBook').modal('hide');
                swal('Great!', 'Book has been updated.', 'success');
                // reload datatable
                getBooks.reload();
              })
              .catch(function (error) {
                console.log(error);
                swal('Something went wrong', 'Unable to update book. Please check your input.', 'error');
              });
        }
    });

    //when button update Info is clicked
    $(document).on('click', '.btnUpdInfo', (e) => {
        e.preventDefault();

        var row = $(e.target).closest('.m-datatable__row');
        var prodId = row.find('[data-field="BookId"] label:last-child').prop('id');

        var bkName = row.find('[data-field="name"] span:last-child').text();
        var bkAuthor = row.find('[data-field="attrib"] span.author:last-child').text();
        var bkPrice = accounting.unformat(row.find('[data-field="price"] h4:last-child').text());
        var bkStock = row.find('[data-field="stock"] h4:last-child').text();

        fillUpmodal({id: prodId, title: ' Update Book Details', name: bkName, author: bkAuthor, price: bkPrice, stock: bkStock});
    });

    //when button remove book is clicked
    $(document).on('click', '.btnRmBook', (e) => {
        e.preventDefault();
        var id = $(e.target).closest('.m-datatable__row').find('[data-field="BookId"] label:last-child').prop('id');

        swal({
            title: "Are you sure?",
            text: "You want to remove this book? This will delete the book from students cart too.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, remove book."
        }).then(function(e) {

            if(e.value) {
                axios.delete('/api/product/id/'+ id)
                .then(resp => {
                    swal('Success!', 'Book has been removed from the system.', 'success');
                    // reload datatable
                    getBooks.reload();
                }).catch(err => {
                    console.error(err);
                    swal('Something went wrong', 'Unable to remove this book. Please try again later.', 'error');
                });
            }
        });
    });
});
