<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Student extends Authenticatable
{
    use Notifiable;

    use Uuids;

    protected $user;
    /**
     * Set auto-increment to false.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'stud_no', 'course', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // parse items to remove uniform detail and update uniform price
    public static function parsedItems($items) {
        $parsedItems = [];

        foreach($items as $item) {
            if ($item->category == 'uniforms') {
                foreach(json_decode($item->detail) as $detail) {
                    if($detail->size->key == $item->unif_key) {
                        $item->price = $detail->price;
                    }
                }
            } 
           unset($item->detail);  // remove detail
           array_push($parsedItems, $item); // push parsedItems
        }

        return $parsedItems;
    }
   
   
    // get student cart items
    public static function getCartItems() {

        $items = DB::table('shopping_cart')->join('products', 'shopping_cart.prod_id', '=', 'products.id')
         ->selectRaw("shopping_cart.*, products.name, products.imgsrc, products.price, products.category, products.attrib->'details' AS detail")->where('stud_id', Auth::id())->latest()->get();  
        
        return self::parsedItems($items);
    }

     // get student mini cart items limited by 3
     public static function getMiniCartItems() {
        
        $items = DB::table('shopping_cart')->join('products', 'shopping_cart.prod_id', '=', 'products.id')
        ->selectRaw("shopping_cart.*, products.name, products.imgsrc, products.price, products.category, products.attrib->'details' AS detail")->where('stud_id', Auth::id())->limit(3)->latest()->get();
        
        return self::parsedItems($items);
    }
}
