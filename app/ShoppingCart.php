<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShoppingCart extends Model
{
    use Uuids;
    /**
     * Set auto-increment to false.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * Table Properties
     */
    protected $table = 'shopping_cart';
    protected $fillable = ['stud_id', 'prod_id', 'qty', 'unif_key'];

    // get uniform available stock
    public static function getUniformStock($cartId) {

       return  DB::table(DB::raw("(
        SELECT shopping_cart.id AS cart_id, shopping_cart.unif_key, jsonb_array_elements(products.attrib -> 'details') AS detail
        from shopping_cart INNER JOIN products ON shopping_cart.prod_id = products.id) AS A"))
        ->selectRaw("detail ->> 'stock' AS stock")->whereRaw("cart_id = ? AND (detail ->'size' ->>'key') = unif_key", $cartId)->first()->stock;
    }
}
