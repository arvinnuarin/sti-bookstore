<?php
namespace App\Helpers;

use Akaunting\Money\Money;
use App\Student;

class Cart
{
    public static function items() {
        return Student::getCartItems();
    }

    public static function miniCartItems() {
        return Student::getMiniCartItems();
    }

    public static function price(float $price) {
        return Money::PHP($price*100);
    }

    public static function amount(float $price, int $qty) {
        return Money::PHP($price*100)->multiply($qty);
    }

    public static function summary() {
        
        $items = self::items();

        $subTotal = 0; $tax = 0; $total = 0; $itemCount = 0;
        
        foreach($items as  $item) {

            $itemCost = floatval($item->price) * intval($item->qty);
            
            $total+= $itemCost;
            $itemCount += intval($item->qty);
        }

         $subTotal = $total / 1.12; $tax = $total - $subTotal;

        return ['count' => $itemCount, 'subtotal' => round($subTotal,2), 'tax' =>  round($tax,2), 'total' =>  round($total,2)];
    }
}
