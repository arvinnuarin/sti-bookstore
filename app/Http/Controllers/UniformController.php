<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Products;

class UniformController extends Controller
{
    public function show($type) {
        if (Auth::guard('admin')->check()) {
            $types = ['subject', 'wash-day-shirts', 'college'];
            $typeTitle = array('subject' => 'Subject Uniform', 'wash-day-shirts' => 'Wash Day Shirt', 'college' => 'Daily School Uniform');
            $sizes = ['S'=> 'Small', 'M' => 'Medium', 'L' => 'Large', 'XL' => 'XL', '2XL' => '2XL', '3XL' => '3XL'];
            $courses = ['SHS' => 'Senior High School', 'BSBM' => 'BS in Business Management',
            'BSCS' => 'BS in Computer Science', 'BSIT' => 'BS in Information Technology',
            'BSCpE' => 'BS in Computer Engineering', 'TEM' => 'Tourism and Events Management',
            'ACT' => 'Associate in Computer Technology', 'HRS' => 'Hospitality and Restaurant Services', 'GEN' => 'General'];

            if(in_array($type, $types)) {

                $title = $typeTitle[$type];
                return view('admin.products.uniforms.index', compact('title', 'type', 'sizes', 'courses'));
            } else {
                abort('404');
            }
        } else {
            return redirect('/login');
        }
    }

    // add school uniform
    public function create(Request $req) {

        try {

            $validator = \Validator::make($req->all(), [
                'name' => 'required|string', 'course' => 'required|string',
                'type' => 'required|string', 'gender' => 'required|string',
                'price' => 'required|numeric', 'details' => 'required|array|min:1',
                'imgsrc' => 'required', 'stock' => 'required|numeric'
            ]);

            if ($validator->passes()) {

                $attribs = json_encode(['type' => $req->type, 'course' => $req->course, 'gender' => $req->gender, 'details' => $req->details]);
                $uniform = Products::create(['category' => 'uniforms', 'name' => $req->name,
                'attrib' => $attribs, 'price' => $req->price, 'stock' => $req->stock, 'imgsrc' => $req->imgsrc]);

                return response()->json($uniform);
            } else {
                return response('Invalid Request.', 400)->header('Content-Type', 'text/plain');
            }
        } catch(Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    // update school uniform
    public function update(Request $req) {

        try {

            $validator = \Validator::make($req->all(), [
                'id' => 'required', 'name' => 'required|string', 'course' => 'required|string',
                'type' => 'required|string', 'gender' => 'required|string',
                'price' => 'required|numeric', 'details' => 'required|array|min:1', 
                'stock' => 'required|numeric'
            ]);

            if ($validator->passes()) {

                $attribs = json_encode(array('type' => $req->type, 'course' => $req->course, 'gender' => $req->gender, 'details' => $req->details));
                $uniform = Products::find($req->id)->update(['name' => $req->name, 'attrib' => $attribs, 'stock' => $req->stock, 'price' => $req->price]);

                return response()->json($uniform);
            } else {
                return response('Invalid Request.', 400)->header('Content-Type', 'text/plain');
            }
        } catch(Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

}
