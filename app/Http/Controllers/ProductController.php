<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use JD\Cloudder\Facades\Cloudder;

class ProductController extends Controller
{
    // get products in a certain category
    public function getCatProduct($category, $subcat) {
        try {
            $cats = ['books', 'uniforms', 'supplies'];
            $sub_cats = ['books' => ['shs-grade-11', 'shs-grade-12'],
            'uniforms' => ['subject', 'wash-day-shirts', 'college']];

            if(in_array($category, $cats)) {

                if(in_array($subcat, $sub_cats['books']) && $category == 'books') {
                    return response()->json(Products::where('category', $category)->where('attrib->grade', $subcat)->orderBy('name')->get());
                }
                elseif(in_array($subcat, $sub_cats['uniforms']) && $category == 'uniforms') {
                    return response()->json(Products::where('category', $category)->where('attrib->type', $subcat)->orderBy('name')->get());
                }
                else if ($subcat == 'supply'&& $category == 'supplies') {
                    return response()->json(Products::where('category', $category)->orderBy('name')->get());
                } else {
                    return response('Subcategory not found.', 404)->header('Content-Type', 'text/plain');
                }
            } else {
                return response('Category not found.', 404)->header('Content-Type', 'text/plain');
            }
        }
        catch (Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }
    // get product by product Id
    public function getProduct($prodId) {

        try {
            $validator = \Validator::make(['uuid' => $prodId], ['uuid' => 'uuid']);

            if ($validator->passes()) {
                return response()->json(Products::find($prodId));
            } else {
                return response('Invalid UUID parameter.', 400)->header('Content-Type', 'text/plain');
            }
        }
        catch (Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    public function uploadImage(Request $req) {
        //return response($req);
        Cloudder::upload($req->file('product')->getRealPath(), null);
        return Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=> 400]);
    }

    //Delete Product
    public function removeProduct($prodId) {

        try {

            $validator = \Validator::make(['uuid' => $prodId], ['uuid' => 'uuid']);

            if ($validator->passes()) {
                $prod = Products::find($prodId);
                $prod->delete();
            } else {
                return response('Incorrect UUID format.', 400)->header('Content-Type', 'text/plain');
            }
            return response('Product has been deleted successfully', 200)->header('Content-Type', 'text/plain');
        } catch(Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }
}
