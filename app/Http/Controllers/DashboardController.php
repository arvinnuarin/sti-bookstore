<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use App\ShopOrders;
use App\Student;


use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct() { // protect routes only authenticated user
        $this->middleware('auth:admin');
    }

    public function show() {

        $pending = ShopOrders::select('summary')->where('isFulfilled', false);
        $fulfilled = ShopOrders::select('summary')->where('isFulfilled', true);

        $orders_cnt = ['pending' => $pending->count(),'fulfilled' => $fulfilled->count(), 'expired' => 0];
        $orders_total = ['pending' =>ShopOrders::sum($pending->get()), 'fulfilled' => ShopOrders::sum($fulfilled->get())];
        $studW = ShopOrders::join('students', 'students.id', 'shop_orders.stud_id')->groupBy('students.id')->distinct()->count('students.id');
        $user_cnt = ['stud' => Student::count(), 'studW'=> $studW, 'adm' => Admin::count()];

        return view('admin.dashboard.index', compact('orders_cnt', 'orders_total', 'user_cnt'));
    }

    /* 
    * Settings 
    */

    public function showProfile() {

        $admin = Auth::guard('admin')->user();
        return view('admin.settings.profile', compact('admin'));
    }

    public function updatePass(Request $req) {

        $validator = \Validator::make($req->all(), [
            'curpass' => 'required|string', 'password' => 'required|string|min:6|confirmed']);
        
        if ($validator->passes()) {

            $admin = Auth::guard('admin')->user();
          
            if(Hash::check($req->curpass, $admin->password)) {                     
                
                $a = Admin::find($admin->id);
                $a->password = \Hash::make($req->password);
                $a->save();
                return response('Admin Password has been updated', 200);
            } else {           
                $error = array('current-password' => 'Please enter correct current password');
                return response()->json(array('error' => $error), 400);   
            } 
        } else {
            return response('Invalid request', 400);
        } 
    }

    // Show Add Admin page
    public function showAddAdmin() {
        return view('admin.settings.admin');
    }
    // retrieve list of admins
    public function getAdmins() {
        $admins = Admin::all();
        return response()->json($admins);
    }

    public function addAdmin(Request $req) {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|string', 'username' => 'required|string|min:6', 'email' => '
            required|email']);
        
        if ($validator->passes()) {
            Admin::create(['name' => $req->name, 'username' => $req->username, 'email' => $req->email, 
            'password' => Hash::make('sample2019')]);

            return response('Successful', 201);
        } else {
            return response('Incorrect data.', 400);
        }
    }
}
