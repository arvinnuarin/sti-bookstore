<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ShoppingCart;
use App\Student;
use App\Products;
use Cart;

class CartController extends Controller
{

    public function __construct() { 
        $this->middleware('auth');
    }
    
    public function show() {

        $items = Student::getCartItems();
        $summary = Cart::summary();
        $sizes = ['uni_S'=> 'Small', 'uni_M' => 'Medium', 'uni_L' => 'Large', 'uni_XL' => 'XL', 'uni_1XL' => '1XL',  'uni_2XL' => '2XL', 'uni_3XL' => '3XL'];

        return view('shop.cart.index', compact('items', 'summary', 'sizes'));
    }

    public function addToCart(Request $req) {

        try {
            $validator = \Validator::make($req->all(), ['prodId' => 'required|string', 'prodType' => 'required|string']);
            $studId = Auth::id();

            if ($validator->passes()) {
                
                // check if item type is uniform
                if($req->prodType == 'uniforms') {
                    // check if uniform size variation already in cart
                    $unifItem = ShoppingCart::where('stud_id', $studId)->where('prod_id', $req->prodId)->where('unif_key', $req->var_key)->first();
                    
                    if(!$unifItem) { // no records
                        $cart = ShoppingCart::create(['stud_id' => $studId, 'prod_id' => $req->prodId, 'qty' => 1, 
                        'unif_key' => $req->var_key ]);
                    } else { // update qty
                        
                        $unif_stock = ShoppingCart::getUniformStock($unifItem->id);

                          // maximum MOQ is 10 for uniform or the available stock
                          $maxQty = 10;

                          if($unif_stock < 10) { $maxQty = $unif_stock; }
                        
                            if(($unifItem->qty + 1) <= $maxQty) {
                                $cart = $unifItem->update(['qty' => intval($unifItem->qty) + 1]);
                            } else {
                                return response('Maximum QTY reached', 400);
                            }
                    }
                } else { // if not uniform

                     // check if item already exists in cart
                    $cartItem = ShoppingCart::where('stud_id', $studId)->where('prod_id', $req->prodId)->first();
                    $cart = null;
                    if(!$cartItem) { // if not cart item
                        $cart = ShoppingCart::create(['stud_id' => $studId, 'prod_id' => $req->prodId, 'qty' => 1]);
                    } else {
                        
                        $prod_stock = Products::find($req->prodId)->stock;
                        // maximum MOQ is 10 or the available stock
                        $maxQty = 10;

                        if($prod_stock < 10) { $maxQty = $prod_stock; }
                        
                        if(($cartItem->qty+1) <= $maxQty) {
                            $cart = ShoppingCart::find($cartItem->id)->update(['qty' => intval($cartItem->qty) + 1]);
                        } else {
                            return response('Maximum QTY reached', 400);
                        }
                    }
                }

               return response()->json($cart);
            }
        } catch(Exception $e) {
            return response()->json($e);
        }
    }

    public function removeToCart($cartId) {
        ShoppingCart::find($cartId)->delete();
        return response('Item has been deleted', 200)->header('Content-Type', 'text/plain');
    }

    public function updateItemQty($cartId, $opt, $inpQty) {
        $cart = ShoppingCart::find($cartId);

        $product = Products::find($cart->prod_id);
        $prod_stock = $product->stock;
        $prod_type = $product->category;

        // maximum MOQ is 10 or the available stock
        $maxQty = 10;

        // get available stocks
        if($prod_type == 'uniforms') {
            $unif_stock = ShoppingCart::getUniformStock($cartId);
            if($unif_stock < 10) { $maxQty = $unif_stock; }
        } else {
            if($prod_stock < 10) { $maxQty = $prod_stock; }
        }

        if($opt == 'more') {
            if(($cart->qty + 1) <= $maxQty) {
                $newCart = $cart->update(['qty' => intval($cart->qty) + 1]);
            } else {
                return response('Maximum QTY reached', 400);
            }
        } elseif($opt == 'less') {
            if(($cart->qty - 1) >= 1) {
                $newCart = $cart->update(['qty' => intval($cart->qty) - 1]);
            } else {
                return response('Not allowed to update qty consider removing item.', 401);
            }
        } else {
            if(intval($inpQty) <= $maxQty) {
                $newCart = $cart->update(['qty' => intval($inpQty)]);
            } else {
                return response('An error occured.', 400);
            }
        }

        return response()->json($newCart);
    }
    // Show Checkout
    public function showCheckout() {
        return view('shop.checkout.index');
    }
}
