<?php

namespace App\Http\Controllers\Auth;

use App\Student;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class StudentRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    // Display Registration Form
    public function register() {

        $courses = ['SHS' => 'Senior High School', 'BSBA' => 'BS in Business Management',
            'BSCS' => 'BS in Computer Science', 'BSIT' => 'BS in Information Technology',
            'BSCpE' => 'BS in Computer Engineering', 'TEM' => 'Tourism and Events Management',
            'ACT' => 'Associate in Computer Technology', 'HRS' => 'Hospitality and Restaurant Services'];

        return view('shop.auth.register', compact('courses'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * 
     * @return \Illuminate\Http\Request
     */
    public function registerStudent(Request $req) {
        
        // validate form data
        $this->validate($req, ['stud_no' => 'required|string', 'name' => 'required|string|max:100',
        'course' => 'required|string', 'gender' => 'required|string', 
        'email' => 'required|string|email|max:100|unique:students', 'username' => 'required|string|max:20|unique:students',
        'password' => 'required|string|min:6|confirmed']);

        // create student
        try {
            $stud = Student::create(['stud_no' => $req->stud_no, 'name' => $req->name, 'course' => $req->course, 
            'gender' => $req->gender, 'email' => $req->email, 'username' => $req->username, 
            'password' => Hash::make($req->password)]);
        } catch(Exception $e) {
            abort(500); 
        }
      
        if($stud) {
            return redirect()->intended(route('register.success', $stud->id));
        } else {
            return redirect()->back()->withInput($req->only('email', 'name', 'username'));
        }
    }

    // Display registration successful
    public function registerSuccess($studId) {

        try {

            $validator = \Validator::make(['uuid' => $studId], ['uuid' => 'uuid']);

            if ($validator->passes()) {
                $stud = Student::find($studId);

                if($stud->exists()) {
                    return view('shop.auth.regsuccess');
                } else {
                    abort(404);
                }
            } else {
                abort(404);
            }
        } catch(Exception $e) {
            abort(404);
        }
    }

}
