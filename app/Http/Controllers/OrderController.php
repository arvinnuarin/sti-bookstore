<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ShopOrders;
use App\Student;
use App\ShoppingCart;
use App\Products;
use Cart;
use Carbon\Carbon;

class OrderController extends Controller
{
    // show admin order page
    public function show($type) {
        if (Auth::guard('admin')->check()) {
            $types = ['pending', 'completed'];
            $typeTitle = array('pending' => 'Pending Orders', 'completed' => 'Completed Orders');

            if(in_array($type, $types)) {

                $title = $typeTitle[$type];
                return view('admin.transactions.orders.index', compact('title', 'type'));
            } else {
                abort('404');
            }
        } else {
            return redirect('/sti-adm');
        }
    }

    public function showCheckout() { // show checkout page

        //retrieve items from user cart;
        $items = Student::getCartItems();
        $summary = Cart::summary();
        $studId = Auth::id();
        $expiration = Carbon::now()->addWeek()->toDateTimeString();

        $order_cnt = sprintf("%04s", ShopOrders::count() + 1);
        $order_no = date('Y').'-'.$order_cnt;

        // check if student cart is not empty
        if(!empty($items)) {
            // create order
            $order = ShopOrders::create(['order_no' => $order_no, 'stud_id' => $studId, 'items' => json_encode($items), 'summary' => json_encode($summary), 
            'isFulfilled' => false, 'expiration' => $expiration]);
            // empty student cart
            ShoppingCart::where('stud_id', $studId)->delete();

           return view('shop.checkout.index', compact('order'));
        } else {
            abort(403);
        } 
    }

    public function showStudentOrderPDF($orderId) {
        
        try {

            $validator = \Validator::make(['uuid' => $orderId], ['uuid' => 'uuid']);

            if($validator->passes()) {

                $order = ShopOrders::getOrder($orderId);

                if($order->stud_id == Auth::id()) {
                    $pdf = \PDF::loadView('pdf.order',['order' => $order]);
                    return $pdf->setOption('zoom', 0.8)->inline('order_'.$order->id.'.pdf');
                } else {
                    abort(403);
                }
            } else {
                abort(500);
            }

        } catch(Exception $e) {
            abort(500);
        }
    }

    public function showAdmStudentOrderPDF($orderId) {
        
        try {

            $validator = \Validator::make(['uuid' => $orderId], ['uuid' => 'uuid']);

            if($validator->passes()) {

                $order = ShopOrders::getOrder($orderId);

                $pdf = \PDF::loadView('pdf.order',['order' => $order]);
                return $pdf->setOption('zoom', 0.8)->inline('order_'.$order->id.'.pdf');
            } else {
                abort(500);
            }

        } catch(Exception $e) {
            abort(500);
        }
    }

    /*
    * API
    */
    public function getAllOrders($status) {

        $validStatus = ['pending', 'completed'];
        $statuses = ['pending' => false, 'completed' => true];

        if(in_array($status, $validStatus)) {
            try{

                $orders = ShopOrders::getAllOrders($statuses[$status]);
                return response()->json($orders);
                
            } catch(Exception $e) {
                return response('An error occured', 500);
            }
        } else {
            return response('Malformed URL.', 404);
        }
    }

    //Fulfill Order
    public function fulfillOrder($orderId) {
        try {

            $validator = \Validator::make(['uuid' => $orderId], ['uuid' => 'uuid']);

            if($validator->passes()) {

                $order = ShopOrders::find($orderId);

                if(!$order->isFulfilled) { // check if order is not fulfilled or in pending status
                    foreach(json_decode($order->items) as $item) {
                        $prod = Products::find($item->prod_id);
                        $new_stock = intval($prod->stock) - intval($item->qty);
    
                        if($item->category == 'uniforms') {
                            
                            $unif_dtl_data = []; // uniform detail data
                            // loop to uniform detail
                            $attrib = json_decode($prod->attrib);
                            foreach($attrib->details as $detail) {
                                if($detail->size->key == $item->unif_key) {
                                    // set new stock
                                    $new_stock = intval($detail->stock) - intval($item->qty);
                                    $detail->stock = $new_stock;
                                }
                                array_push($unif_dtl_data, $detail);
                            }
                            // set new uniform data
                              $unif_data = json_encode(['type'=>$attrib->type, 'course'=> $attrib->course, 'gender'=>$attrib->gender, 'details'=> $unif_dtl_data]);
                              $prod->update(['attrib' => $unif_data]);
                        } 
                       $prod->update(['stock' => $new_stock]);
                    }
                    $order->update(['isFulfilled' => true]);
                }
                
                return response()->json($order);
            } else {
                return response('An error occured. Order ID not found.', '400');
            }

        } catch(Exception $e) {
            return response('An error occured. Order ID not found.', '400');
        }
    }

    // Show Student Orders
    public function showStudOrders() {

        $orders = ShopOrders::select('id', 'order_no', 'created_at', 'summary->total AS amount','isFulfilled')->where('stud_id', Auth::id())->latest()->get();
        return view('shop.orders.index', compact('orders'));
    }
}
