<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Student;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    public function __construct() { 
        $this->middleware('auth');
    }

    public function showHome() {
        return view('shop.home');
    }

    // get category products
    public function getProducts($category, $subcat) {
        try {
            $cats = ['books', 'uniforms', 'school-supplies'];
            $sub_cats = ['books' => ['shs-grade-11', 'shs-grade-12'],
            'uniforms' => ['subject', 'wash-day-shirts', 'college']];

            $bookTitle = ['shs-grade-11' => 'SHS Grade 11 Books List', 'shs-grade-12' => 'SHS Grade 12 Books List'];
            $uniformTitle = ['subject' => 'Subject Uniform', 'wash-day-shirts' => 'Wash Day Shirt', 'college' => 'Daily School Uniform'];    

            if(in_array($category, $cats)) {

                $products = []; // init products
                $title = ""; // init page title

                if(in_array($subcat, $sub_cats['books']) && $category == 'books') {
                    $products = Products::where('category', $category)->where('attrib->grade', $subcat)
                    ->where('stock','>=', 1)->orderBy('name')->get();
                    $title = $bookTitle[$subcat];
                }
                elseif(in_array($subcat, $sub_cats['uniforms']) && $category == 'uniforms') {
                    $products = Products::where('category', $category)->where('attrib->type', $subcat)
                    ->where('stock','>=', 1)->orderBy('name')->get();
                    $title = $uniformTitle[$subcat];
                }
                else if ($subcat == 'item' && $category == 'school-supplies') {
                    $products = Products::where('category', 'supplies')->where('stock','>=', 1)->orderBy('name')->get();
                    $title = 'School Supplies List';
                } else {
                    abort(404); // sub category not found
                }
                return view('shop.products.list', compact('title', 'products'));
            } else {
                return response('Category not found.', 404)->header('Content-Type', 'text/plain');
            }
        }
        catch (Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    // Show Profile page
    public function showProfile() {

        $stud = Auth::user();
        return view('shop.profile.index', compact('stud'));
    }

    //Update Student password
    public function updatePass(Request $req) {

        $validator = \Validator::make($req->all(), [
            'curpass' => 'required|string', 'password' => 'required|string|min:6|confirmed']);
        
        if ($validator->passes()) {

            $stud = Auth::user();
 
            if(\Hash::check($req->curpass, $stud->password)) {                     
                
                $s = Student::find($stud->id);
                $s->password = \Hash::make($req->password);
                $s->save();
                return response('Student Password has been updated', 200);
            } else {           
                $error = array('current-password' => 'Please enter correct current password');
                return response()->json(array('error' => $error), 400);   
            } 
        } else {
            return response('Invalid request', 400);
        } 
    }
}