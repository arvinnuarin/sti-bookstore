<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Products;

class SchoolSupplyController extends Controller
{
    public function show() {
        if (Auth::guard('admin')->check()) {
            return view('admin.products.supply.index');
        } else {
            return redirect('/login');
        }
    }

    /*
    * REST API FUNCTIONS
    */

    // add school supply
    public function create(Request $req) {

        try {

            $validator = \Validator::make($req->all(), [
                'name' => 'required|string', 'price' => 'required|numeric|min:1|max:10000', 'imgsrc' => 'required', 
                'stock'=> 'required|numeric|min:1|max:1000'
            ]);

            if ($validator->passes()) {

                $supply = Products::create(['category' => 'supplies', 'name' => $req->name ,'price' => $req->price, 
                'stock'=> $req->stock, 'imgsrc' => $req->imgsrc]);

                return response()->json($supply);
            } else {
                return response('Invalid Request.', 400)->header('Content-Type', 'text/plain');
            }
        } catch(Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    public function update(Request $req) {

        try {

            $validator = \Validator::make($req->all(), [
                'name' => 'required|string', 'price' => 'required|numeric|min:1|max:10000',  'stock'=> 'required|numeric|min:1|max:1000'
            ]);

            if ($validator->passes()) {

                $supply = Products::find($req->id)->update(['name' => $req->name ,'price' => $req->price, 'stock' => $req->stock]);
                return response()->json($supply);
            } else {
                return response('Invalid Request.', 400)->header('Content-Type', 'text/plain');
            }
        } catch(Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }
}
