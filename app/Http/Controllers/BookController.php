<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Products;

class BookController extends Controller
{

    /*
    * VIEWS FUNCTIONS
    */
    public function show($grade) {

        if (Auth::guard('admin')->check()) {
            $grades = ['shs-grade-11', 'shs-grade-12'];
            $gradeTitle = ['shs-grade-11' => 'SHS Grade 11', 'shs-grade-12' => 'SHS Grade 12'];

            if(in_array($grade, $grades)) {

                $title = $gradeTitle[$grade];
                return view('admin.products.books.index', compact('title', 'grade'));
            } else {
                abort(404);
            }
        } else {
            return redirect('/login');
        }
    }

    /*
    * REST API FUNCTIONS
    */

    // adding book
    public function create(Request $req) {
        try {

            $validator = \Validator::make($req->all(), [
                'author' => 'required|string', 'grade' => 'required|string',
                'name' => 'required|string', 'price' => 'required|numeric|min:1|max:10000', 
                'stock' => 'required|numeric|min:1|max:1000', 'imgsrc' => 'required'
            ]);

            if ($validator->passes()) {
                $attribs = json_encode(array('author' => $req->author, 'grade' => $req->grade));
                $book = Products::create(['category' => 'books', 'name' => $req->name,
                'attrib' => $attribs, 'price' => $req->price, 'stock' => $req->stock, 'imgsrc' => $req->imgsrc]);

             return response()->json($book);
            } else {
                return response('Invalid Request.', 400)->header('Content-Type', 'text/plain');
            }
        } catch(Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    // update book
    public function update(Request $req) {

        try {

            $validator = \Validator::make($req->all(), [
                'author' => 'required|string', 'grade' => 'required|string',
                'name' => 'required|string', 'price' => 'required|numeric|min:1|max:10000',
                'stock' => 'required|numeric|min:1|max:1000', 'id' => 'required',
            ]);

            if ($validator->passes()) {
                $attribs = json_encode(array('author' => $req->author, 'grade' => $req->grade));
                $book = Products::find($req->id)->update(['name' => $req->name,
                'attrib' => $attribs, 'price' => $req->price, 'stock'=> $req->stock]);

             return response()->json($book);
            } else {
                return response('Invalid Request.', 400)->header('Content-Type', 'text/plain');
            }
        } catch(Exception $e) {
            return response('Something went wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

}
