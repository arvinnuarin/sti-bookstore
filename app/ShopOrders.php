<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopOrders extends Model
{
    use Uuids;
    /**
     * Set auto-increment to false.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * Table Properties
     */
    protected $table = 'shop_orders';
    protected $fillable = ['order_no', 'stud_id', 'items', 'summary', 'isFulfilled', 'expiration'];

    // Get All Orders
    public static function getAllOrders(bool $isFulfilled) {
        return DB::table('shop_orders')->join('students', 'shop_orders.stud_id', 'students.id')
        ->select('shop_orders.*', 'students.name', 'students.stud_no', 'students.course')->where('shop_orders.isFulfilled', $isFulfilled)
        ->latest()->get();
    }

    public static function getOrder($Orderid) {
        return DB::table('shop_orders')->join('students', 'shop_orders.stud_id', 'students.id')
        ->select('shop_orders.*', 'students.name', 'students.stud_no', 'students.course')->where('shop_orders.id', $Orderid)
        ->first();
    }

    public static function sum($items) {
        $total = 0;

        foreach($items as $item) {
            $summary = json_decode($item->summary);
            $total += doubleval($summary->total);
        }

        return $total;
    }
}
