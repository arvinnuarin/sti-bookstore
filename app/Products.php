<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use Uuids;
    /**
     * Set auto-increment to false.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * Table Properties
     */
    protected $table = 'products';
    protected $fillable = ['category', 'name', 'attrib', 'price', 'stock', 'imgsrc'];
}
