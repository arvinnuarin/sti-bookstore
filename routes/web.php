<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
* ADMIN ROUTES
*/

Route::prefix('sti-adm')->group(function () {

    // Auth Routes
    Route::get('/', 'Auth\AdminLoginController@login')->name('admin.auth.login');
    Route::post('login', 'Auth\AdminLoginController@loginAdmin')->name('admin.auth.loginAdmin');
    Route::get('logout', 'Auth\AdminLoginController@logout')->name('admin.auth.logout');
    // Dashboard Route
    Route::get('dashboard', 'DashboardController@show')->name('admin.dashboard');
    // Products Web Routes
    Route::get('products/books/{grade}', 'BookController@show')->name('admin.products.books');
    Route::get('products/uniforms/{type}', 'UniformController@show')->name('admin.products.uniforms');
    Route::get('products/school-supplies', 'SchoolSupplyController@show')->name('admin.products.supplies');
    // Orders Routes
    Route::get('transactions/orders/{type}', 'OrderController@show');
    Route::get('transactions/orders/pdf/{orderId}', 'OrderController@showAdmStudentOrderPDF');
    // Settings
    Route::get('settings/profile', 'DashboardController@showProfile');
    Route::get('settings/admins', 'DashboardController@showAddAdmin');
});

// Admin web api

Route::prefix('/api')->group(function() {
    // Update Password admin
    Route::post('/settings/admin/password', 'DashboardController@updatePass');
    // Add Admin
    Route::post('/settings/admin', 'DashboardController@addAdmin');
    // Get Admin
    Route::get('/admin/list', 'DashboardController@getAdmins');
});

/*
* SHOP ROUTES
*/

// Auth Routes
Route::get('register', 'Auth\StudentRegisterController@register')->name('register');
Route::post('register', 'Auth\StudentRegisterController@registerStudent');
Route::get('register-success/{studId}', 'Auth\StudentRegisterController@registerSuccess')->name('register.success');
Route::get('login', 'Auth\StudentLoginController@login')->name('login');
Route::post('login', 'Auth\StudentLoginController@loginShop');
Route::get('/', 'ShopController@showHome')->name('shop.home');
Route::get('logout', 'Auth\StudentLoginController@logout');

Route::group(['middleware' => 'web'], function () {
    //Products Route
    Route::get('products/{category}/{subcat}', 'ShopController@getProducts');
    //Cart & Checkout Route
    Route::get('cart', 'CartController@show');
    // Profile Routes
    Route::get('profile', 'ShopController@showProfile');
    // Checkout Route
    Route::get('checkout', 'OrderController@showCheckout');
    Route::get('checkout/pdf/order/{orderId}', 'OrderController@showStudentOrderPDF');
    //Order Route
    Route::get('transaction/orders', 'OrderController@showStudOrders');

    Route::prefix('/api')->group(function() {
        // Cart API
        Route::post('student/cart', 'CartController@addToCart');
        Route::delete('cart/item/{cartId}', 'CartController@removeToCart');
        Route::patch('cart/item/{cartId}/{opt}/{inpQty}', 'CartController@updateItemQty');
        //Update Password
        Route::post('student/password', 'ShopController@updatePass');
    });
});