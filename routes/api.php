<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Products API
Route::get('product/{category}/{subcat}', 'ProductController@getCatProduct');
Route::get('product/search/id/{prodId}', 'ProductController@getProduct');
Route::post('product/image', 'ProductController@uploadImage');
Route::delete('product/id/{prodId}', 'ProductController@removeProduct');

// Book API
Route::post('product/book', 'BookController@create');
Route::patch('product/book', 'BookController@update');

// Uniforms API
Route::post('product/uniform', 'UniformController@create');
Route::patch('product/uniform', 'UniformController@update');

// School Supplies API
Route::post('product/supply', 'SchoolSupplyController@create');
Route::patch('product/supply', 'SchoolSupplyController@update');

// Orders API
Route::get('orders/{status}', 'OrderController@getAllOrders');
Route::patch('orders/id/{orderId}', 'OrderController@fulfillOrder');

