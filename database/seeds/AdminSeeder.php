<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert(['id' => Uuid::generate()->string, 'name' => 'Micaella Oronce', 'username'=> 'mikay',
        'email'=> 'mikay@gmail.com', 'password' => Hash::make('sample2019')]);
    }
}
