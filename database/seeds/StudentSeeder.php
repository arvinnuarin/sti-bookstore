<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert(['id' => Uuid::generate()->string, 'name' => 'Micaella Oronce', 'username'=> 'mikay',
        'email'=> 'mikay@gmail.com', 'password' => Hash::make('student2019'), 'stud_no' => 'STUD-001', 'course' => 'BSIT',
        'gender' => 'FEMALE']);
    }
}
