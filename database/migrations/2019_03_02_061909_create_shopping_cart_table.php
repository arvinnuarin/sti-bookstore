<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_cart', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('stud_id');
            $table->uuid('prod_id');
            $table->integer('qty');
            $table->string('unif_key', 10)->nullable();
            $table->timestamps();

            $table->foreign('stud_id')->references('id')->on('students')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('prod_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_cart');
    }
}
