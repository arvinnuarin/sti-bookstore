<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <title>STI Bookstore | {{ $order->order_no }}</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .page-break {
            page-break-after: always;
        }
        .table > tbody > tr > td {
            vertical-align: middle;
        }
    </style>

</head>
<body>
    <main class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-6">
                        <img src="{{ public_path('/images/logo-bookstore.jpg') }}" height="100px" width="300px">
                        <h4 class="text-muted">1001 Recto Ave, Quiapo, Manila, 1001 Metro Manila</h4>
                        <h4 class="text-muted">contact@sti.edu.ph</h4>
                        <h4 class="text-muted">734-8668 / 736-2233</h4>
                    </div>

                    <div class="col-xs-6">
                        <h1>RESERVATION SLIP</h1>
                        <div class="row">
                            <div class="col-xs-6 text-muted">
                                <h4>Order No:</h4>
                                <h4>Transaction Date:</h4>
                                @if($order->isFulfilled)
                                    <h4>Fulfilled At:</h4>
                                @else
                                    <h4>Valid Until:</h4>
                                @endif
                                <h4>Status:</h4> 
                            </div>
                            <div class="col-xs-6">
                                <h4>{{ $order->order_no }}</h4>
                                <h4>{{ $order->created_at }}</h4>
                                
                                @if($order->isFulfilled)
                                <h4>{{ $order->updated_at }}</h4>
                                    <h4 class="text-primary">FULFILLED</h4> 
                                @else
                                    <h4>{{ $order->expiration }}</h4>
                                    <h4 class="text-warning">PENDING</h4> 
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12" style="margin-top: 50px;">
                <h2>CONSIGNEE</h2>
                <div class="col-xs-3 text-muted">
                    <h4>Student Number:</h4>
                    <h4>Student Name:</h4>
                    <h4>Course / Grade:</h4>
                </div>
                <div class="col-xs-9">
                    <h4>{{ $order->stud_no }}</h4>
                    <h4>{{ $order->name }}</h4>
                    <h4>{{ $order->course }}</h4>
                </div>
            </div>

            <div class="col-xs-12" style="margin-top: 50px;">
                <h2>RESERVED ITEMS</h2>
                <table class="table table-striped table-bordered text-center" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Product Name</th>
                            <th class="text-center">Unit Price</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">Total Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(json_decode($order->items) as $i=>$itm)
                            @if($itm->category == 'uniforms')
                                @php 
                                    $sizes = ['uni_S'=> 'Small', 'uni_M' => 'Medium', 'uni_L' => 'Large', 'uni_XL' => 'XL', 'uni_1XL' => '1XL',  'uni_2XL' => '2XL', 'uni_3XL' => '3XL'];
                                    $itm->name = $itm->name. ' ('.$sizes[$itm->unif_key].')';
                                @endphp
                            @endif
                            
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td style="text-align:left">
                                    <div>
                                        <span>
                                            <img src="{{ $itm->imgsrc }}" height="80px">
                                        </span>
                                        <span style="margin-left: 25px; font-weight:bold">
                                            <label>{{ $itm->name }}</label>
                                        </span>
                                    </div>
                                </td>
                                <td>{{ Cart::price($itm->price) }}</td>
                                <td>{{ $itm->qty }}</td>
                                <td style="font-weight:bold">{{ Cart::amount($itm->price, $itm->qty) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-xs-12" style="margin-top: 30px;">
                <div class="row">
                    <div class="col-xs-7"></div>
                    <div class="col-xs-3 text-muted">
                        <h4># of Items:</h4>
                        <h4>Subtotal:</h4>
                        <h4>VAT (12%):</h4>
                        <h4>Total:</h4>
                    </div>
                    <div class="col-xs-2">
                        @php 
                            $summary = json_decode($order->summary);
                        @endphp
                        <h4>{{ $summary->count }}</h4>
                        <h4>{{ Cart::price($summary->subtotal) }}</h4>
                        <h4>{{ Cart::price($summary->tax) }}</h4>
                        <h4 class="text-danger">{{ Cart::price($summary->total) }}</h4>
                    </div>
                </div>
            </div>



            <div class="col-xs-12 text-center" style="margin-top: 50px;">
                <label>**Please present this reservation slip to the bookstore personnel to facilitate payment and the delivery of your reserved items.**</label>
                <label style="margin-top:20px">**SYSTEM-GENERATED REPORT<label>
                <label>**NO SIGNATURE NECESSARY**</label>
            </div>
        </div>
    </main>
</body>
</html>