@extends('layouts.shop.main')
@section('page-title')
My Orders
@endsection
@section('content')
<section>
    <div class="container mt-50 mb-50">
        <div class="brk-tables brk-tables-trend font__family-montserrat brk-library-rendered rendered" data-brk-library="component__tables">
            <table>
                <thead>
                    <tr>
                        <th scope="col">Order #</th>
                        <th class="brk-tables_active" scope="col">Transaction Date</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Status</th>
                        <th scope="col">View Items</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td data-title="Order" class="align-middle">{{ $order->order_no }}</td>
                            <td data-title="TransactionDate" class="brk-tables_active align-middle">{{ $order->created_at }}</td>
                            <td data-title="Amount" class="align-middle">{{ Cart::price($order->amount) }}</td>
                            <td data-title="Status" class="align-middle">
                                @if($order->isFulfilled)
                                    <h5>COMPLETED</h5>
                                @else 
                                    <h5>PENDING</h5>
                                @endif
                           </td>
                            <td data-title="View" class="align-middle">
                                <a href="/checkout/pdf/order/{{$order->id}}" class="icon__btn icon__btn-md icon__btn-anim brk-library-rendered" data-brk-library="component__button"><i class="fa fa-file-pdf icon-inside" aria-hidden="true"></i>
                                <span class="before"></span><span class="after"></span></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection