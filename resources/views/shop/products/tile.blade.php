<section class="col-xl-3 col-lg-6">
    <div class="flip-box brk-sc-flip-two text-center" data-brk-library="component__shop_flip,component__flip_box_css,scrollbar">
        <div class="flip flip_horizontal">
            <div class="flip__front brk-sc-flip-two__front">
                <div class="brk-sc-flip-two__thumb">{{ $image }}</div>
                <div class="brk-sc-flip-two__description">
                    <div class="brk-sc-flip-two__price font__family-montserrat font__weight-medium brk-base-font-color lblfrontPrice">{{ $price }}</div>
                    <div class="brk-sc-flip-two__front-title font__family-montserrat font__weight-light brk-dark-font-color">{{ $title }}
                        @if($type == 'uniforms')
                        <br>
                        <label class="text-muted lblUnifSize"></label>
                        @endif
                        </div>
                </div>
            </div>
            <div class="flip__back brk-sc-flip-two__back">
                <div class="brk-sc-flip-two__back-layer">
                    <div class="brk-sc-flip-two__back-title font__family-montserrat font__weight-medium">{{ $title }}</div>
                    <div class="brk-sc-flip-two__list scrollbar-inner">
                        <ul class="pl-15 pr-15">
                            {{ $desc }}
                        </ul>
                    </div>
                    <div class="brk-sc-flip-two__button" data-type="{{ $type }}" id="colAddToCart">
                        <button id="{{$prodId}}" class="btn btnAddToCart btn-inside-out btn-md btn-icon border-radius-25 font__family-open-sans font__weight-semibold" data-brk-library="component__button">
                            <i class="far fa-shopping-basket icon-inside"></i><span class="before lblCartBPrice">{{ $price }}</span>
                            <span class="text">Add to Cart</span><span class="after lblCartAPrice">{{ $price }}</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>