@extends('layouts.shop.main')
@section('page-title')
{{ $title }}
@endsection
@section('content')
<main class="main-container mt-30 mt-md-60">
    <section>
        <div class="container">
            <div class="row">
                @foreach($products as $prod)
                    @component('shop.products.tile')
                        @slot('title')
                            {{ $prod->name }}
                        @endslot
                        @slot('image')
                        <img src="{{$prod->imgsrc}}" data-src="{{$prod->imgsrc}}" alt="{{ $prod->name }}" class="lazyload">
                        @endslot
                        @slot('price')
                           @money($prod->price*100, 'PHP')
                        @endslot
                        @slot('prodId')
                          {{ $prod->id }}
                        @endslot
                        @slot('desc')
                            @php 
                                $attrib = json_decode($prod->attrib)
                            @endphp
                            
                            @if($prod->category == 'books')
                                <li><i class="far fa-check brk-base-font-color"></i> Author: {{ $attrib->author }}</li>
                                <li><i class="far fa-check brk-base-font-color"></i> Grade: {{ $attrib->grade }}</li>
                              <!--  <li><i class="far fa-check brk-base-font-color"></i> Available Stock: <span class="text-primary">{{ $prod->stock }}</span></li> -->
                            @elseif($prod->category == 'uniforms')
                                <label>Available Sizes & Stocks:</label>
                                @php 
                                    $numOfCols = 2;
                                    $rowCount = 0;
                                @endphp

                                <div class="row no-gutters btnSizes">
                                    @foreach($attrib->details as $item)
                                        <div class="col-6">
                                            <button data-unifsize="{{$item->size->key}}" class="btn_size btn btn-inside-out btn-sm btn-icon border-radius-25 font__family-open-sans font__weight-bold brk-library-rendered rendered" data-brk-library="component__button">
                                                <span class="before">{{ $item->size->name }}</span><span class="text">{{ Cart::price($item->price) }}</span>
                                                <span class="after">{{ $item->size->name }}</span>
                                               <!-- <span class="before">{{ $item->size->name. ' ('.$item->stock.')' }}</span><span class="text">{{ Cart::price($item->price) }}</span>
                                                <span class="after">{{ $item->size->name. ' ('.$item->stock.')' }}</span> -->
                                            </button>
                                        </div>
                                    @endforeach
                                @php 
                                    $rowCount++;
                                    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                                @endphp
                                <hr>
                                <li><i class="far fa-check brk-base-font-color"></i> Type: {{ $attrib->type }}</li>
                                <li><i class="far fa-check brk-base-font-color"></i> Course: {{ $attrib->course }}</li>
                                <li><i class="far fa-check brk-base-font-color"></i> Gender: {{ $attrib->gender }}</li>
                            @else
                               <!-- <li><i class="far fa-check brk-base-font-color"></i> Available Stock: <span class="text-primary">{{ $prod->stock }}</span></li> -->
                            @endif
                        @endslot
                        @slot('type')
                           {{ $prod->category }}
                        @endslot
                    @endcomponent
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection

@section('page-script')
<script src="/js/page-scripts/shop/product.listing.js"></script>
@endsection