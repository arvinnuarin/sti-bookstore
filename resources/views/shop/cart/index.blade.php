@extends('layouts.shop.main')
@section('page-title')
My Cart
@endsection
@section('content')
<main class="main-container pt-90 mb-10">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-11">
				<div class="tabs-wrapper mb-120" data-brk-library="component__steps">
					<div class="steps__wrapper-main steps__wrapper-checkers">
						<div class="steps__progress"></div>
						<ul class="steps__wrapper tabs" data-tabgroup="tab-group-5">
							<li class="complete"><a class="active"><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 1</span>
									<p class="font__family-montserrat font__size-15 steps__title">Cart</p>
								</a></li>
								<li class=""><a href="/checkout" class=""><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 2</span>
										<p class="font__family-montserrat font__size-15 steps__title">Checkout</p>
									</a></li>
								<li class=""><a class=""><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 4</span>
										<p class="font__family-montserrat font__size-15 steps__title">Print Slip</p>
									</a></li>
								<li class=""><a class=""><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 3</span>
										<p class="font__family-montserrat font__size-15 steps__title">Payment</p>
									</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
            <!-- cart -->
			<div class="brk-cart" data-brk-library="component__cart">
				<table>
					<thead>
						<tr>
							<th colspan="2">Product</th>
							<th>Unit Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
                        <!-- cart items -->
						@foreach($items as $item)
							@component('shop.cart.item')
								@slot('imgsrc')
									<button id="{{$item->id}}" class="brk-cart__remove btnRemove"></button>
									<img data-src="{{$item->imgsrc}}" src="{{$item->imgsrc}}" alt="{{$item->name}}" class="lazyload brk-cart__img">
								@endslot
								@slot('name')
								<a href="{{'/products/'.$item->category}}" class="font__size-16 line-height-1-375 brk-cart__title">{{ $item->name }}</a>
									@if($item->category == 'uniforms')
										<span class="font__size-16 line-height-1-375 brk-cart__title font__weight-bold">({{ $sizes[$item->unif_key] }})</span>
									@endif
								@endslot
								@slot('id')
									{{ $item->id }}
								@endslot
								@slot('price')
									{{ Cart::price($item->price) }}
								@endslot
								@slot('qty')
									{{ $item->qty }}
								@endslot
								@slot('total')
								<span class="font__size-16 line-height-1-5 font__family-montserrat font__weight-bold lblItemTotal">
									
									{{ Cart::amount(floatval($item->price), intval($item->qty)) }}
								</span>
								@endslot
							@endcomponent
						@endforeach
					</tbody>
                </table>
                <!-- cart footer -->
                <div class="brk-cart__footer">
                    <div class="brk-cart__footer-coupon">
                    </div>
                    <div class="brk-cart__footer-total">
						<div class="brk-cart__footer-total-item font__family-montserrat">
                            <div class="font__size-sm-14 font__size-13 line-height-1-5">Subtotal</div>
                            <div class="font__size-14 line-height-1-5 font__weight-bold" id="lblCartSubTotal">{{ Cart::price($summary['subtotal']) }}</div>
                        </div>
                        <div class="brk-cart__footer-total-item font__family-montserrat">
                            <div class="font__size-sm-14 font__size-13 line-height-1-5">Tax (12%)</div>
                            <div class="font__size-14 line-height-1-5 font__weight-bold" id="lblCartTax">{{ Cart::price($summary['tax']) }}</div>
                        </div>
                        <div class="brk-cart__footer-total-item font__family-montserrat">
                            <div class="font__size-sm-14 font__size-13 line-height-1-5">Total Amount</div>
                            <div class="font__size-16 line-height-1-25 font__weight-bold brk-base-font-color" id="lblCartTotal">{{ Cart::price($summary['total']) }}</div>
                        </div>
                    </div>
                </div>
            <div class="d-flex justify-content-center flex-md-row flex-column pt-md-30 pt-30 pb-md-30 pb-30 pl-lg-60 pl-30 pr-30">
				<a href="/cart" class="btn btn-inside-out btn-inside-out-invert btn-lg btn-icon border-radius-30 font__family-open-sans font__size-13 font__family-montserrat font__weight-bold pl-50 pr-50" data-brk-library="component__button">
					<i class="fas fa-sync-alt icon-inline"></i> <span class="before">Update cart</span>
					<span class="text">Update cart</span><span class="after">Update cart</span>
				</a>
				<a href="/checkout" class="btn btn-inside-out btn-lg btn-icon border-radius-30 font__family-open-sans font__weight-semibold btn-icon-right pl-60 pr-90 mt-md-0 mt-30" data-brk-library="component__button">
					<i class="fas fa-long-arrow-alt-right icon-inside"></i> <span class="before">Checkout</span>
					<span class="text">Checkout</span><span class="after">Checkout</span>
				</a>
			</div>
        </div>
    </div>
</main>
@endsection

@section('page-script')
<script src="/js/page-scripts/shop/cart.js"></script>
@endsection