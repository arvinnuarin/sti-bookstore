<tr>
    <td>
        <div class="d-flex flex-row justify-content-lg-start justify-content-between align-items-center">
            {{ $imgsrc }}
        </div>
    </td>
    <td aria-label="Product">
        {{ $name }}
    </td>
    <td aria-label="Unit Price">
        <span class="font__size-16 line__height-20 font__family-montserrat font__weight-bold">{{ $price }}</span>
    </td>
    <td aria-label="Quantity">
        <div class="brk-cart__count brk-cart__count_input" id="{{ $id }}">
            <button type="button" class="count-less"><i class="fas fa-caret-left brk-base-font-color"></i></button><input type="number" class="txtQty" value="{{ $qty }}" min="1" max="20">
            <button type="button" class="count-more"><i class="fas fa-caret-right brk-base-font-color"></i></button>
        </div>
    </td>
    <td aria-label="Total">
        {{ $total }}
    </td>
</tr>