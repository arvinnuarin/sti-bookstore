@extends('layouts.shop.main')
@section('page-title')
My Profile
@endsection
@section('content')
<main class="main-container mb-50 mt-70">
	<div class="row">
        <div class="col-12 mb-30">
            <div class="row">
                <div class="col-lg-7 col-sm-6">
                    
                </div>
                <div class="col-lg-5">
                    <a href="/transaction/orders" class="btn btn-prime btn-lg border-radius-25 font__family-open-sans font__weight-bold btn-min-width-200 brk-library-rendered" data-brk-library="component__button">
                        <span class="before"></span><span class="after"></span><span class="border-btn"></span>My Orders</a>
                    <a href="/logout" class="btn btn-prime btn-lg border-radius-25 font__family-open-sans font__weight-bold btn-min-width-200 brk-library-rendered" data-brk-library="component__button">
                    <span class="before"></span><span class="after"></span><span class="border-btn"></span>Logout</a>
                </div>
            </div>
        </div>
        
        <!--Profile Information-->
        <div class="col-lg-6 col-md-12 col-xs-12">
            <section>
				<div class="container mb-70">
					<div class="text-left">
						<h3 class="font__family-montserrat font__size-28 font__weight-bold"><span class="heading__number">01</span> About Me</h3>
					</div>
				</div>
				<div class="container mb-120">
					<div class="brk-form brk-form-round brk-library-rendered" data-brk-library="component__form">
                        <div class="row">
                            <div class="col-lg-8 col-xs-12 col-md-8 mb-50">
                                <label class="brk-form-label font__family-montserrat font__weight-bold">Name</label>
                                <h4 class="ml-30">{{ $stud->name }}</h4>
                            </div>
                            <div class="col-lg-8 col-xs-12 col-md-8 mb-50">
                                <label class="brk-form-label font__family-montserrat font__weight-bold">Student Number</label>
                                <h4 class="ml-30">{{ $stud->stud_no }}</h4>
                            </div>
                            <div class="col-lg-8 col-xs-12 col-md-8 mb-50">
                                <label class="brk-form-label font__family-montserrat font__weight-bold">Course</label>
                                <h4 class="ml-30">{{ $stud->course }}</h4>
                            </div>
                            <div class="col-lg-8 col-xs-12 col-md-8 mb-50">
                                <label class="brk-form-label font__family-montserrat font__weight-bold">Email Address</label>
                                <h4 class="ml-30">{{ $stud->email }}</h4>
                            </div>
                        </div>
                    </div>
				</div>
			</section>
        </div>
        <!--Update password-->
        <div class="col-lg-6 col-md-12 col-xs-12">
            <section>
				<div class="container mb-70">
					<div class="text-left">
						<h3 class="font__family-montserrat font__size-28 font__weight-bold"><span class="heading__number">02</span> Update Password</h3>
					</div>
				</div>
				<div class="container mb-120">
					<div class="brk-form brk-form-round brk-library-rendered" data-brk-library="component__form">
						<form id="frmUpdatePass">
                            <div class="row">
                                <div class="col-lg-8 col-xs-12 col-md-8 mb-50">
                                    <label class="brk-form-label font__family-montserrat font__weight-bold" for="brk-email-form">Current Password</label>
                                    <input name="curpass" type="password" placeholder="">
                                </div>
                                <div class="col-lg-8 col-xs-12 col-md-8 mb-50">
                                    <label class="brk-form-label font__family-montserrat font__weight-bold" for="brk-email-form">New Password</label>
                                    <input name="password" type="password" placeholder="">
                                </div>
                                <div class="col-lg-8 col-xs-12 col-md-8 mb-50">
                                    <label class="brk-form-label font__family-montserrat font__weight-bold" for="brk-email-form">Confirm Password</label>
                                    <input name="password_confirmation" type="password" placeholder="">
                                </div>
                                <div class="col-lg-8 col-sm-8">
                                    <button type="button" id="btnUpdatePass" class="btn btn-prime btn-lg btn-icon-abs border-radius-0 font__family-open-sans font__weight-bold btn-min-width-200 brk-library-rendered" data-brk-library="component__button">
                                    <span class="before"></span><span class="after"></span><span class="border-btn"></span>Update Password</button>
                                </div>
                            </div>
						</form>
                    </div>
				</div>
			</section>
        </div>
    </div>
</main>
@endsection

@section('page-script')
<script src="/js/page-scripts/shop/profile.js"></script>
@endsection

