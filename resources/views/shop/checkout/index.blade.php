@extends('layouts.shop.main')
@section('page-title')
Order Checkout
@endsection
@section('content')
<main class="main-container pt-90 mb-10">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-11">
				<div class="tabs-wrapper mb-120" data-brk-library="component__steps">
					<div class="steps__wrapper-main steps__wrapper-checkers">
						<div class="steps__progress"></div>
						<ul class="steps__wrapper tabs" data-tabgroup="tab-group-5">
							<li class="complete"><a class="active"><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 1</span>
									<p class="font__family-montserrat font__size-15 steps__title">Cart</p>
								</a></li>
								<li class="complete"><a class="active"><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 2</span>
										<p class="font__family-montserrat font__size-15 steps__title">Checkout</p>
									</a></li>
								<li class=""><a class=""><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 4</span>
										<p class="font__family-montserrat font__size-15 steps__title">Print Slip</p>
									</a></li>
								<li class=""><a class=""><span class="font__family-montserrat font__weight-bold font__size-15 steps__dot"><span class="before"></span> <span class="after"></span> 3</span>
										<p class="font__family-montserrat font__size-15 steps__title">Payment</p>
									</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
            <!-- cart -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center mb-50">
                       <h3>Your order has been successfully reserved. This reservation will auto expire in 7 days.</h3>
                    </div>
                    <div class="col-sm-12 text-center text-muted">
                       <label>Kindly print the reservation slip and present it to the bookstore upon payment.</label>
                    </div>
                </div>
            </div>
				
            <!-- checkout footer -->    
            <div class="d-flex justify-content-center flex-md-row flex-column pt-md-30 pt-30 pb-md-30 pb-30 pl-lg-60 pl-30 pr-30">
                <a href="/checkout/pdf/order/{{$order->id}}" class="btn btn-inside-out btn-inside-out-invert btn-lg btn-icon border-radius-30 font__family-open-sans font__size-13 font__family-montserrat font__weight-bold pl-50 pr-50" data-brk-library="component__button">
                <i class="fas fa-print"></i>
                <span class="before"> Print Reservation Slip</span><span class="text"> Print Reservation Slip</span><span class="after"> Print Reservation Slip</span>
                </a>
            </div>
        </div>
    </div>
</main>
@endsection