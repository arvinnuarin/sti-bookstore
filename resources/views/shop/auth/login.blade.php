@extends('layouts.shop.auth')
@section('page-title')
    Student Login
@endsection
@section('content')
<div class="col-12 col-lg-10">
    <h1 class="font__family-montserrat font__weight-bold font__size-42 line__height-42 mt-0 mb-45 text-center text-lg-left">STUDENT LOGIN</h1>
    <form action="/login" method="POST" class="brk-form brk-form-strict maxw-570 mx-auto mx-lg-0" data-brk-library="component__form">
            {{ csrf_field() }}
            @if ($errors->has('username'))
                <div class="alert alert-danger">
                    <strong>{{ $errors->first('username') }}</strong>
                </div>
            @elseif ($errors->has('password'))
                <div class="alert alert-danger">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
            @endif

        <input type="text" placeholder="Username" name="username">
        <input type="password" placeholder="Password" name="password">
        <div class="mt-30 d-flex flex-wrap justify-content-between align-items-center flex-column flex-lg-row">
            <button type="submit" class="btn-backgrounds btn-backgrounds btn-backgrounds_280 btn-backgrounds_white btn-backgrounds_left-icon font__family-montserrat font__weight-bold text-uppercase font__size-13 z-index-2 text-center letter-spacing-20 mt-10" data-brk-library="component__button">
                <span class="text">Login Now</span> 
                <span class="before"><i class="far fa-hand-point-right"></i></span>
            </button>
            <a href="/register" class="btn-backgrounds btn-backgrounds btn-backgrounds_280 btn-backgrounds_white font__family-montserrat font__weight-bold text-uppercase font__size-13 z-index-2 text-center letter-spacing-20 mt-10" data-brk-library="component__button">
                <span class="text">Sign up</span> <span class="before"><i class="fas fa-user"></i></span>
            </a>
        </div>
    </form>
</div>
@endsection