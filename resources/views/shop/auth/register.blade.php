@extends('layouts.shop.auth')
@section('page-title')
    Register
@endsection
@section('content')
<div class="col-12 col-lg-12">
    <form action="/register" method="POST" class="brk-form brk-form-strict" data-brk-library="component__form">
        {{ csrf_field() }}

        @if ($errors->has('name'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('name') }}</strong>
            </div>
        @elseif ($errors->has('stud_no'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('stud_no') }}</strong>
            </div>
        @elseif ($errors->has('email'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @elseif ($errors->has('username'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('username') }}</strong>
            </div>
        @elseif ($errors->has('password'))
        <div class="alert alert-danger">
            <strong>{{ $errors->first('password') }}</strong>
        </div>
        @endif

        <div class="row">
            <div class="col-6">
                <input type="text" placeholder="Full Name" name="name">
                <input type="text" placeholder="Student Number" name="stud_no">
                <select class="form-control m-input" id="sltGender" name="gender">
                    <option value="MALE">MALE</option>
                    <option value="FEMALE">FEMALE</option>
                </select>
                <input type="email" placeholder="Email Address" name="email">
            </div>
            <div class="col-6">
                <input type="text" placeholder="Username" name="username">
                <select class="form-control m-input" id="sltCourse" name="course">
                    @foreach($courses as $key => $course)
                        <option value="{{ $key }}">{{ $course }}</option>
                    @endforeach
                </select>
                <input type="password" placeholder="Password" name="password">
                <input type="password" placeholder="Confirm Password" name="password_confirmation">
            </div>
        </div>

        <div class="d-flex flex-wrap justify-content-between align-items-center flex-column flex-lg-row mt-5">
            <button type="submit" class="btn-backgrounds btn-backgrounds btn-backgrounds_280 btn-backgrounds_white btn-backgrounds_left-icon font__family-montserrat font__weight-bold text-uppercase font__size-13 z-index-2 text-center letter-spacing-20 mt-10" data-brk-library="component__button">
                <span class="text">Register</span> 
                <span class="before"><i class="far fa-hand-point-right"></i></span>
            </button>
            <a href="/login" class="btn-backgrounds btn-backgrounds btn-backgrounds_280 btn-backgrounds_white font__family-montserrat font__weight-bold text-uppercase font__size-13 z-index-2 text-center letter-spacing-20 mt-10" data-brk-library="component__button">
                <span class="text">Log In</span> <span class="before"><i class="fas fa-user"></i></span>
            </a>
        </div>
    </form>
</div>
@endsection