@extends('layouts.shop.auth')
@section('page-title')
    Registration Success
@endsection
@section('content')
<div class="col-12 col-lg-10">
    
    <div class="row text-center">
        <h3>Thank you for registering your account. You may now login.</span>
        <div class="mt-5">
            <a href="/login" class="btn-backgrounds btn-backgrounds btn-backgrounds_280 btn-backgrounds_white font__family-montserrat font__weight-bold text-uppercase font__size-13 z-index-2 text-center letter-spacing-20 mt-10" data-brk-library="component__button">
                <span class="text">Log In</span> <span class="before"><i class="fas fa-user"></i></span>
            </a>
        </div>
    </div>
</div>
@endsection