@extends('layouts.shop.main')
@section('page-title')
Home
@endsection
@section('content')
@include('shop.home.revSlider')

@endsection

@section('page-script')
<<script src="vendor/revslider/js/jquery.themepunch.tools.min.js"></script>
	<script src="vendor/revslider/js/jquery.themepunch.revolution.min.js"></script>
	<script src="vendor/revslider/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script src="vendor/revslider/js/extensions/revolution.extension.navigation.min.js"></script>
	<script src="vendor/revslider/js/extensions/revolution.extension.parallax.min.js"></script>
	<script src="vendor/revslider/js/extensions/revolution.extension.slideanims.min.js"></script>
<!--<script src="/js/page-scripts/shop/revslider.settings.js"></script>-->
<script>
    var revapi18,
        tpj;
    (function() {
        if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
        else onLoad();

        function onLoad() {
            if (tpj === undefined) {
                tpj = jQuery;
                if ("off" == "on") tpj.noConflict();
            }
            if (tpj("#rev_slider_18_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_18_1");
            } else {
                revapi18 = tpj("#rev_slider_18_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "vendor/revslider/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 6000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        arrows: {
                            style: "gyges",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 991,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            tmp: '',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 20,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 20,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            hide_over: 992,
                            style: "hebe",
                            hide_onleave: false,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 20,
                            space: 5,
                            tmp: '<span class="tp-bullet-image"></span>'
                        }
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1200, 992, 768, 576],
                    gridheight: [950, 768, 1024, 815],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 400,
                        speedbg: 0,
                        speedls: 0,
                        levels: [5, 8, 11, 14, 17, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
                    },
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }; /* END OF revapi call */
        }; /* END OF ON LOAD FUNCTION */
    }()); /* END OF WRAPPING FUNCTION */
</script>
@endsection