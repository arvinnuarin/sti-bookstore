@extends('layouts.dashboard.main')
@section('page-title')
    Manage Admin Users
@endsection
@section('dash-content')
<!-- Table Portlet -->
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    STI Online Bookstore Admin List
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <button class="btn btn-focus m-btn m-btn--custom m-btn--icon m-btn--air" id="btnToggleModal"  data-toggle="modal" data-target="#mdlAddAdmin">
                        <span>
                            <i class="la la-user"></i>
                            <span>New Admin User</span>
                        </span>
                    </button>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div>
        </div>
        <!--end: Search Form -->

        <!--begin: Datatable -->
        <div class="m_datatable" id="tblAdmins"></div>
        <!--end: Datatable -->
    </div>
</div>

@include('admin.settings.mdlAdmin');

@endsection

@section('page-script')
<script src="/js/page-scripts/admin/admin.js"></script>
@endsection