@extends('layouts.dashboard.main')
@section('page-title')
    My Profile
@endsection
@section('dash-content')
<div class="row">
    <!--Information-->
    <div class="col-lg-6 col-md-6 col-xs-12">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text" id="supplies" data-field="type">
                           Information
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label>Name</label>
                    <h3>{{ $admin->name }}</h3>
                </div>
                <div class="form-group m-form__group">
                    <label>Email Address</label>
                    <h3>{{ $admin->email }}</h3>
                </div>
                <div class="form-group m-form__group">
                    <label>Username</label>
                    <h3>{{ $admin->username }}</h3>
                </div>
            </div>
        </div>
    </div>
    <!--Update Password-->
    <div class="col-lg-6 col-md-12 col-xs-12">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text" id="supplies" data-field="type">
                           Update Password
                        </h3>
                    </div>
                </div>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right" id="frmUpdatePass">
				<div class="m-portlet__body">
					<div class="form-group m-form__group">
						<label>Current Password</label>
						<input type="password" class="form-control m-input" name="curpass" placeholder="Current Password">
					</div>
                    <div class="form-group m-form__group">
						<label>New Password</label>
						<input type="password" class="form-control m-input" name="password" placeholder="New Password">
					</div>
                    <div class="form-group m-form__group">
						<label>Confirm Password</label>
						<input type="password" class="form-control m-input" name="password_confirmation" placeholder="Confirm Password">
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="button" class="btn btn-primary" id="btnUpdatePass">Update Password</button>
					</div>
				</div>
			</form>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/js/page-scripts/admin/profile.js"></script>
@endsection