<!-- Modal -->
<div class="modal fade" id="mdlAddAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" data-field="uuid">
                    <span><i class="la la-user"></i><span id="mdlTitle">Add Admin User</span></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class ="m-form m-form--fit" id="frmNewAdmin">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Full Name:</label>
                            <div class="col-lg-8">
                                <input type="text" name="name" class="form-control m-input" placeholder="Input full name." required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Email Address:</label>
                            <div class="col-lg-8">
                                <input type="email"  name="email" class="form-control m-input" placeholder="Input email address." required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Username:</label>
                            <div class="col-lg-8">
                                <input type="text"  name="username" class="form-control m-input" placeholder="Input username." required>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnAddAdmin">Save changes</button>
            </div>
        </div>
    </div>
</div>
