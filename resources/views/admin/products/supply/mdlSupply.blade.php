<!-- Modal -->
<div class="modal fade" id="mdlSupply" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" data-field="uuid" id="">
                    <span><i class="la la-pencil"></i><span>Add School Supply</span></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-form__section m-form__section--first">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Name:</label>
                        <div class="col-lg-8">
                            <input type="text" id="txtName" class="form-control m-input" placeholder="Input school supply name." required>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Price:</label>
                        <div class="col-lg-4">
                            <input type="number" id="txtPrice" class="form-control m-input" placeholder="0.00" required min="1" max="10000">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Available Stock:</label>
                        <div class="col-lg-4">
                            <input type="number" id="txtStock" class="form-control m-input" value="1" required min="1" max="1000" required>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4">Image:</label>
                        <div class="col-lg-8">
                            <input  id="prdImage" type="file" name="image" accept="image/*" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSupplySave">Save changes</button>
            </div>
        </div>
    </div>
</div>
