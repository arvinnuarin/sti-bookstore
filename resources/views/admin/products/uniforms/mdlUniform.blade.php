<!-- Modal -->
<div class="modal fade" id="mdlUniform" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" data-field="uuid">
                    <span><i class="fas fa-tshirt"></i><span id="mdlTitle"> Add {{ $title }}</span></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-form__section m-form__section--first">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Name:</label>
                        <div class="col-lg-8">
                            <input type="text" id="txtName" class="form-control m-input" placeholder="Input Uniform name." required>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Course:</label>
                        <div class="col-lg-8">
                        <select class="form-control m-input" id="sltCourse">
                            @foreach($courses as $key => $course)
                                <option value="{{ $key }}">{{ $course }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Gender:</label>
                        <div class="col-lg-8">
                            <div class="m-radio-list">
                                <label class="m-radio m-radio--state-success">
                                    <input type="radio" name="gender" value="Male"> Male
                                    <span></span>
                                </label>
                                <label class="m-radio m-radio--state-brand">
                                    <input type="radio" name="gender" value="Female"> Female
                                    <span></span>
                                </label>
                                <label class="m-radio m-radio--state-primary">
                                    <input type="radio" name="gender" value="Unisex" checked="checked"> Unisex
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- Uniform Details-->
                    <div class="col-lg-12">
                    <table class="table m-table m-table--head-bg-brand">
                        <thead>
                            <tr class="text-center">
                                <th style="width: 35%">Size</th>
                                <th>Price</th>
                                <th>Stocks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sizes as $key => $size)
                                <tr>
                                    <td class="align-middle text-center">{{ $size }}</td>
                                    <td class="align-middle">
                                        <input id="{{ 'uni_'.$key }}" type="number" class="form-control m-input uniPrice" placeholder="0.00" required min="1" max="10000">
                                    </td>
                                    <td class="align-middle">
                                        <input id="{{ 'stk_uni_'.$key }}" type="number" class="form-control m-input uniStock" value="1" required min="1" max="1000">
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4">Image:</label>
                        <div class="col-lg-8">
                            <input  id="prdImage" type="file" name="image" accept="image/*" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnUniformSave">Save changes</button>
            </div>
        </div>
    </div>
</div>
