<!-- Modal -->
<div class="modal fade" id="mdlNewBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" data-field="uuid">
                    <span><i class="la la-book"></i><span id="mdlTitle">{{' New '.$title.' Book'}}</span></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class ="m-form m-form--fit">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Book Name:</label>
                            <div class="col-lg-8">
                                <input type="text" id="txtBkName" name="name" class="form-control m-input" placeholder="Input book name." required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Author / Publisher:</label>
                            <div class="col-lg-8">
                                <input type="text"  id="txtBkAuthor" name="author" class="form-control m-input" placeholder="Input book author." required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Price:</label>
                            <div class="col-lg-4">
                                <input type="number" id="txtBkPrice" name="price" class="form-control m-input" placeholder="0.00" required min="1" max="10000">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Available Stock:</label>
                            <div class="col-lg-4">
                                <input type="number" id="txtBkStock" name="stock" class="form-control m-input" required min="1" max="1000" value="1">
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-4">Book Image:</label>
                            <div class="col-lg-8">
                                <input  id="prdImage" type="file" name="image" accept="image/*" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnAddBookSave">Save changes</button>
            </div>
        </div>
    </div>
</div>
