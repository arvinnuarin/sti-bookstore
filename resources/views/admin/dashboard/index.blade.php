@extends('layouts.dashboard.main')
@section('page-title')
    Dashboard
@endsection
@section('dash-content')
<div class="m-portlet">
    <div class="m-portlet__body m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-12 col-lg-12 col-xl-4">
                <!--begin:: Widgets/Stats2-1 -->
                <div class="m-widget1">
                    <!--Orders-->
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">New Orders</h3>
                                <span class="m-widget1__desc">Orders that is currently pending</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-brand">{{ $orders_cnt['pending'] }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Fulfilled Orders</h3>
                                <span class="m-widget1__desc">Completed Orders</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-danger">{{ $orders_cnt['fulfilled'] }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Expired Orders</h3>
                                <span class="m-widget1__desc">Orders that are abandoned</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-success">{{ $orders_cnt['expired'] }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Order Amount -->
            <div class="col-md-12 col-lg-12 col-xl-4">
                <!--begin:: Widgets/Stats2-1 -->
                <div class="m-widget1">
                    <!--Orders-->
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">New Orders</h3>
                                <span class="m-widget1__desc">Total Amount of Orders</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-brand">{{ Cart::price($orders_total['pending']) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Fulfilled Orders</h3>
                                <span class="m-widget1__desc">Total Amount</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-danger">{{ Cart::price($orders_total['fulfilled']) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Expired Orders</h3>
                                <span class="m-widget1__desc">Total Amount of Orders</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-success">{{ Cart::price(0) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Count -->
            <div class="col-md-12 col-lg-12 col-xl-4">
                <!--begin:: Widgets/Stats2-1 -->
                <div class="m-widget1">
                    <!--Orders-->
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Reg. Student</h3>
                                <span class="m-widget1__desc">Registered Shop Students</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-brand">{{ $user_cnt['stud'] }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Reg. Student</h3>
                                <span class="m-widget1__desc">with orders</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-danger">{{ $user_cnt['studW'] }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Reg. Admins</h3>
                                <span class="m-widget1__desc">Registered Shop Admins</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-success">{{ $user_cnt['adm'] }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection