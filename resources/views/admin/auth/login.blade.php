@extends('layouts.app')
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--desktop m-grid--ver-desktop m-grid--hor-tablet-and-mobile m-login m-login--6" id="m_login">
        <div class="m-grid__item   m-grid__item--order-tablet-and-mobile-2  m-grid m-grid--hor m-login__aside " style="background-image: url(/images/adminbg.png);">
            <div class="m-grid__item">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="/images/logo-bookstore.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver">
                <div class="m-grid__item m-grid__item--middle">
                    <span><h3>STI Bookstore Management System</h3></span>
                    <span><h3>This portal is for AUTHORIZED PERSONNEL only.</h3></span>
                </div>
            </div>
            <div class="m-grid__item">
                <div class="m-login__info">
                    <div class="m-login__section">
                        <span><h3>&copy 2019 STI RECTO</h3></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-grid__item m-grid__item--fluid  m-grid__item--order-tablet-and-mobile-1  m-login__wrapper">

            <!--begin::Body-->
            <div class="m-login__body">

                <!--begin::Signin-->
                <div class="m-login__signin">
                    <div class="m-login__title">
                        <h3>User Authentication</h3>
                    </div>

                    <!--begin::Form-->
                    <form class="m-login__form m-form" method="POST" action="{{ route('admin.auth.loginAdmin') }}" >
                    {{ csrf_field() }}

                        @if ($errors->has('username'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('username') }}</strong>
                            </div>
                        @elseif ($errors->has('password'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif

                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="Username" name="username" autocomplete="off">
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                        </div>
                        <!--begin::Action-->
                        <div class="m-login__action">
                            <button type="submit" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">Sign In</button>
                        </div>
                        <!--end::Action-->
                    </form>

                    <!--end::Form-->
                </div>

                <!--end::Signin-->
            </div>

            <!--end::Body-->
        </div>
    </div>
</div>
@endsection
