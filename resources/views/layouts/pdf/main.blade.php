<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <title>STI Bookstore | @yield('page-title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/shop/app.css') }}" rel="stylesheet">
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>

</head>
<body>
    <div class="container">
        @yield('main-content')
    </div>
</body>
</html>