<footer class="brk-footer position-relative" data-brk-library="component__footer">
    <div class="brk-footer__wrapper pt-50"><span class="brk-abs-overlay brk-base-bg-gradient-left-100"></span>
        <div class="container">
            <div class="brk-footer__wide-menu pt-20 pt-sm-15 pb-15 d-flex flex-wrap justify-content-between align-items-center">
                <p class="font__size-14 line__height-21 font__weight-normal text-sm-left mx-xs-auto">&#xA9; 2019 STI Recto All rights reserved</p>
                <nav class="brk-footer-nav pt-10 pt-md-0">
                    <ul class="d-flex flex-wrap justify-content-center font__size-14 font__weight-medium text-uppercase">
                        <li><a href="/">home</a></li>
                        <li><a href="/products/books/shs-grade-11">books</a></li>
                        <li><a href="/products/uniforms/subject">uniforms</a></li>
                        <li><a href="/products/school-supplies/item">school supplies</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>


<a href="#top" id="toTop"></a>
