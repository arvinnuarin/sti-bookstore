<div class="main-page">
    <div class="brk-header-mobile">
        <div class="brk-header-mobile__open brk-header-mobile__open_white"><span></span></div>
        <div class="brk-header-mobile__logo"><a href="/"><img src="/images/shop/logo-dark-2.png" alt=""></a></div>
    </div>
    <header class="brk-header brk-header_style-1 brk-header_color-white brk-header_skin-1 position-fixed d-lg-flex flex-column" style="display: none;" data-logo-src="/images/shop/logo-dark-2.png" data-bg-mobile="/images/shop/brk-bg-mobile-menu.jpg" data-brk-library="component__header">
        <div class="brk-header__main-bar brk-header_border-bottom-20" style="height: 72px;">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-auto pl-25 align-self-lg-center d-none d-lg-block">
						<div class="text-center"><a href="/" class="brk-header__logo brk-header__item @@modifier">
							<img class="brk-header__logo-1 lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="/images/shop/logo-1.png" alt="alt">
							<img class="brk-header__logo-2 lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="/images/shop/logo-dark-1.png" alt="alt"></a>
						</div>
                    </div>
                    <div class="col-lg align-self-lg-stretch">
                        <nav class="brk-nav brk-header__item">
                            <ul class="brk-nav__menu">
                                <li><a href="/"><span>Home</span></a></li>
                                <li class="brk-nav__children brk-nav__drop-down-effect"><a href="#"><span>Books</span></a>
                                    <ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
                                        <li class="dd-effect"><a href="/products/books/shs-grade-11">Senior High School Grade 11</a></li>
                                        <li class="dd-effect"><a href="/products/books/shs-grade-12">Senior High School Grade 12</a></li>
                                    </ul>
                                </li>
                                <li class="brk-nav__children brk-nav__drop-down-effect"><a href="#"><span>Uniforms</span></a>
                                    <ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
                                        <li class="dd-effect"><a href="/products/uniforms/subject">Subject Uniforms</a></li>
										<li class="dd-effect"><a href="/products/uniforms/wash-day-shirts">Wash-day Shirts</a></li>
										<li class="dd-effect"><a href="/products/uniforms/college">College Uniforms</a></li>
                                    </ul>
                                </li>
                                <li><a href="/products/school-supplies/item"><span>School Supplies</span></a></li>
							</ul>
						</nav>
					</div>
					<div class="col-lg-auto align-self-lg-stretch text-right">
						@include('layouts.shop.minicart')

						<div class="brk-header__element brk-header__element_skin-1 brk-header__item">
							<a href="/profile" class="brk-header__element--wrap"><i class="fal fa-user"></i>
							<span class="brk-header__element--label d-lg-none">Profile</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
</div>
@if(Route::current()->getName() != 'shop.home')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="/images/shop/1920x258_1.jpg" data-brk-library="component__breadcrumbs_css"><span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
	<div class="breadcrumbs__wrapper">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12">
					<div class="breadcrumbs__title">
						<h2 class="brk-white-font-color font__weight-semibold font__size-48 line__height-68 font__family-montserrat">@yield('page-title')</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
