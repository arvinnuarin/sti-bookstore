<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#2775FF">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'STI Bookstore |'}} @yield('page-title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/shop/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/shop/styles.min.css') }}" rel="stylesheet">

</head>
<body class="">
    <div id="app" class="main-wrapper">
        @yield('content')
    </div>
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('/js/shop/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/js/shop/scripts.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('page-script')
</body>
</html>
