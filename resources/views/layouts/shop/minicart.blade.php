<div class="brk-mini-cart brk-header__item">
    @php
        $miniCartItems = Cart::miniCartItems();
        $miniSummary = Cart::summary();
    @endphp
    
    <div class="brk-mini-cart__info-open">
        <div class="brk-mini-cart__info-open-thumb">
            <div class="thumb-1 lazyload" data-bg="/images/shop/shop-basket.png">
                <img src="/images/shop/shop-basket.png">
            </div>
            <span class="brk-mini-cart__info-open-count">{{ $miniSummary['count'] }}</span>
        </div>
        <div class="brk-mini-cart__info-open-total-price font__family-montserrat font__weight-semibold">{{ Cart::price($miniSummary['total']) }}</div>
    </div>
    <div class="brk-mini-cart__menu">
        <div class="brk-mini-cart__header"><span class="font__family-montserrat font__weight-bold font__size-18">Your Shopping Cart</span></div>
        <div class="brk-mini-cart__products">
            @foreach($miniCartItems as $item)
                @component('layouts.shop.minicartitem')
                    @slot('image')
                        <img class="lazyload" src="{{$item->imgsrc}}" data-src="{{$item->imgsrc}}" alt="{{$item->name}}">
                    @endslot
                    @slot('details')
                        <a href="{{'/products/'.$item->category}}"><h4 class="font__family-montserrat font__size-16 line__height-21 font__weight-semibold text-truncate">{{ $item->name }}</h4></a>
                        <span class="brk-mini-cart__product--price font__family-montserrat">
                            {{ Cart::amount(floatval($item->price), intval($item->qty)) }}
                        </span>
                    @endslot
                    @slot('qty')
                        {{ $item->qty }}
                    @endslot
                    @slot('id')
                        {{ $item->id }}
                    @endslot
                @endcomponent
            @endforeach
        </div>
        <div class="brk-mini-cart__links">
            <div class="brk-mini-cart__links--wrap">
                <a class="brk-mini-cart__links--cart" href="/cart"><i class="fa fa-shopping-basket"></i> Cart</a>
                <a class="brk-mini-cart__links--checkout" href="/checkout">Proceed to checkout<i class="far fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>