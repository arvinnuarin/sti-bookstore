<div class="brk-mini-cart__product">
    <div class="brk-mini-cart__product--img">
        {{ $image }}
    </div>
    <div class="brk-mini-cart__product--title-price">
        {{ $details }}
    </div>
    <div>
        <label>Qty:</label>
        <h5>{{ $qty }}</h5>
    </div>
    <button class="brk-mini-cart__product--remove remove btnRemoveMiniItem" id="{{$id}}">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
    </button>
</div>