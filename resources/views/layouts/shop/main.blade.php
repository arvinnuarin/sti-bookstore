<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="brk-preloader" lang="en" data-brk-skin="brk-yellow.css">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#2775FF">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>STI Bookstore | @yield('page-title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/shop/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/shop/styles.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/vendor/revslider/css/settings.css">
    <link rel="stylesheet" href="/vendor/toastr/toastr.min.css">
</head>
<body class="">
    <div id="app">
        @include('layouts.shop.header')
        @yield('content')
        @include('layouts.shop.footer')
    </div>
    <!-- Scripts -->
    
    <script src="{{ asset('js/shop/scripts.js') }}"></script>
    <script src="{{ asset('js/shop/app.js') }}"></script>
    <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript">
        var removeMiniItem = (cartId) => {
            Swal.fire({
                title: 'Are you sure?', text: "You want to remove this item from your cart?", type: 'question',
                showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, remove Item!'
            }).then(res => {

                if(res.value) {
                    axios.delete('/api/cart/item/' + cartId).then(res => {
                        location.reload();
                    }).catch(err => {
                        console.log(err);
                        Swal.fire('Sorry!', 'Unable to remove the item in your cart at this moment.', 'error');
                    });
                }
            });
        };

        $(document).ready(() => {

            $('.btnRemoveMiniItem').on('click', (e) => {
                removeMiniItem($(e.target).prop('id'));
            });
        });
    </script>
    @yield('page-script')
</body>
</html>
