<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#2775FF">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'STI Bookstore |'}} @yield('page-title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/shop/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/shop/styles.min.css') }}" rel="stylesheet">

</head>
<body class="">
    <div class="main-wrapper" id="app">
		<main class="main-container">
			<section class="">
				<div class="container-fluid">
					<div class="row row-no-gutter">
						<div class="col-12 col-lg-5 d-lg-block d-none">
							<div class="full-screen position-relative d-flex flex-column justify-content-center align-items-center z-index-2">
                            
                                <div class="brk-backgrounds brk-base-bg-gradient-15 brk-abs-overlay" data-brk-library="component__backgrounds_css,component__backgrounds_js,assets_particleground">
									<div class="brk-backgrounds__canvas brk-particles-standart"></div>
                                    <div class="row text-center h-100">
                                        <div class="col-sm-12 my-auto">
                                            <img src="/images/logo-bookstore.jpg" alt="logo">
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
						<div class="col-12 col-lg-7">
							<div class="full-screen d-flex align-items-center pt-30 pb-30 pt-lg-0 pb-lg-0">
								<div class="container-fluid">
									<div class="row justify-content-lg-start justify-content-center">
										<div class="col-lg-2 d-none d-lg-block"></div>
										@yield('content')
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
    </div>
    <!--To top-->
    <a href="#top" id="toTop"></a>
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('/js/shop/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/js/shop/scripts.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('page-script')
</body>
</html>
