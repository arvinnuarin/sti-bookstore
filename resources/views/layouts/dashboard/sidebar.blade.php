<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <!--Dashboard-->
            <li class="m-menu__item " aria-haspopup="true">
                <a href="/sti-adm" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                    <span class="m-menu__link-text">Dashboard</span>
                </a>
            </li>
            <!--Separator-->
            <li class="m-menu__section ">
                <h4 class="m-menu__section-text">Product Items</h4>
                <i class="m-menu__section-icon flaticon-more-v2"></i>
            </li>
            <!--Books-->
            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon la la-book"></i><span class="m-menu__link-text">Books</span><i
                        class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">STI Bookstore</span></span></li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/products/books/shs-grade-11" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Senior HS Grade 11</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/products/books/shs-grade-12" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Senior HS Grade 12</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <!--School Uniform-->
            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon la la-shirtsinbulk"></i><span class="m-menu__link-text">School Uniform</span><i
                        class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">STI Bookstore</span></span></li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/products/uniforms/subject" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Subject Uniforms</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/products/uniforms/wash-day-shirts" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Wash-day Shirts</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/products/uniforms/college" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Daily School Uniforms</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <!--School Supplies-->
            <li class="m-menu__item " aria-haspopup="true">
                <a href="/sti-adm/products/school-supplies" class="m-menu__link ">
                    <i class="m-menu__link-icon la la-pencil"></i>
                    <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                    <span class="m-menu__link-text">School Supplies</span>
                </a>
            </li>
            <!--Transactions-->
            <li class="m-menu__section ">
                <h4 class="m-menu__section-text">Transactions</h4>
                <i class="m-menu__section-icon flaticon-more-v2"></i>
            </li>
            <!--Orders-->
            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">Orders</span><i
                        class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">STI Bookstore</span></span></li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/transactions/orders/pending" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Pending Orders</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/transactions/orders/completed" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Completed Orders</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
             <!--Settings-->
             <li class="m-menu__section ">
                <h4 class="m-menu__section-text">Settings</h4>
                <i class="m-menu__section-icon flaticon-more-v2"></i>
            </li>
            <!--Settings-->
            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon la la-gears"></i><span class="m-menu__link-text">Settings</span><i
                        class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">STI Bookstore</span></span></li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/settings/profile" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">My Profile</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="/sti-adm/settings/admins" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Manage Admin Users</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>

<!-- END: Left Aside -->
